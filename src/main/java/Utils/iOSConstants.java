package Utils;

public class iOSConstants {


    // initial settings
    public final static String PLATFORM_NAME = "iOS";
    public final static String DEVICE_NAME = "76D45B23-5D33-45A2-B963-509539410D57";
    public final static String DEVICE_TYPE = "iOS Simulator";
    public final static String BUNDLE_ID = "com.solidict.akk";
    public final static String AUTOMATION_NAME = "XCUITest";
    public final static String URL = "http://0.0.0.0:4723/wd/hub";
    public final static String UDID = "76D45B23-5D33-45A2-B963-509539410D57";
    public final static String PLATFORM_VERSION = "11.0";

    // login info
    public final static String MSISDN = "5343481090";
    public final static String PASSWORD = "018530";
    public final static String FALSE_MSISDN = "5343481091";
    public final static String FALSE_PASSWORD = "018531";

    // sign up info
    public final static String SIGNUP_NAME = "Mustafa";
    public final static String SIGNUP_SURNAME = "Nevzat";

    // search
    public final static String SEARCH_EXAMPLE = "a";
    public final static String SEARCH_EXAMPLE_SECOND = "b";

    // contact us
    public final static String EXAMPLE_TEXT = "merhaba";

    // settings
    public final static String TEST_EMAIL = "abc@cde.com";

    // opportunity verification vc
    public final static String VERIFY_TC = "26966056362";
    public final static String VERIFY_NAME = "Veli";
    public final static String VERIFY_SURNAME = "Simit";
    public final static String VERIFY_BIRTHDAY = "30/12/1987";

    // edit share
    public final static String CAPS_TEXT = "deneme";
    public final static String TEXT_EDIT_TEXT = "deneme";

    // mykids
    public final static String NEW_KID_NAME = "Kamuran Yerebakan";
    public final static String CHILD_HEIGHT = "111";
    public final static String CHILD_WEIGHT = "11";

}
