package Utils;

public class SimIOSData {


    // Accessibility ID's

    // Login Splash
    public static String accessibilityID_LoginSplash_Login_Button="loginSplashLogin";
    public static String accessibilityID_LoginSplash_Signup_Button="loginSplashSignUp";

    // Login
    public static String accessibilityID_textField_GSMNumber= "phoneTextField";
    public static String accessibilityID_textField_GSMPassword= "passwordTextField";
    public static String accessibilityID_Login_LoginButton="tapToLogin";
    public static String accessibilityID_Login_Bg_Splash_JPG="bg_splash.jpg";
    public static String accessibilityID_Signup_Login_Header="KAYIT OL";

    // Signup
    public static String accessibilityID_textFieldName="textfieldName";
    public static String accessibilityID_textFieldSurname="textfieldSurname";
    public static String accessibilityID_textFieldBirthday="textfieldBirthday";
    public static String accessibilityID_pickerBirthday="pickerBirthday";
    public static String accessibilityID_pickerDoneButton="Tamam";
    public static String accessibilityID_selectFemaleutton="buttonFemaleSelect";
    public static String accessibilityID_selectMaleButton="buttonManSelect";
    public static String accessibilityID_continueButton="buttonSignUp";

    // Home
    public static String accessibilityID_Home_MenuIcon = "homeLeftMenu";
    public static String accessibilityID_Home_GiftBox = "icon ustbar firsatlarim";
    public static String accessibilityID_Home_ForMe_Button = "forMeButton";
    public static String accessibilityID_Home_ForKids_Button = "forKidsButton";
    public static String accessibilityID_Home_ForHome_Button = "forHomeButton";

    // Left Menu
    public static String accessibilityID_LeftMenu_Home = "Ana Sayfa";
    public static String accessibilityID_LeftMenu_Opportunities = "Ayrıcalıklarım";
    public static String accessibilityID_LeftMenu_CikisButton = "Çıkış";
    public static String accessibilityID_PopUpCikisYapButton = "popupLogoutButton";
    public static String accessibilityID_Help = "Yardım";
    public static String accessibilityID_Settings = "Ayarlar";
    public static String accessibilityID_Hesabim = "Hesabım";
    public static String accessibilityID_Notifications = "Bildirimler";
    public static String accessibilityID_SelectFromLibrary = "Kütüphaneden Seç";

    // MyKids
    public static String accessibilityID_addChild_Button = "button add";
    public static String accessibilityID_haveChild_Button = "ocuumVarBTN";
    public static String accessibilityID_pregnant_Button = "hamileyimBTN";
    public static String accessibilityID_male_Button = "Erkek";
    public static String accessibilityID_female_Button = "Kız";
    public static String accessibilityID_continue_Kids_Button = "DEVAM";
    public static String accessibilityID_Kids_Popup_OK_Button = "OK";
    public static String accessibilityID_Kids_NameSurname_Textfield = "kidNameSurnameTextfield";
    public static String accessibilityID_Kids_See_Development = "gelisimKarnesiBut";
    public static String accessibilityID_Kids_Remove_Child = "icon remove";
    public static String accessibilityID_Kids_Add_Data_Child = "addNewEntryForKidButton";
    public static String accessibilityID_Child_Height = "heightInfo";
    public static String accessibilityID_Child_Weight = "weightInfo";
    public static String accessibilityID_Update_ChildInfo_Button = "updateChildInfo";
    public static String accessibilityID_No_Gender = "Henüz Bilmiyorum";

    // Opportunity
    public static String accessibilityID_OpportunityVC_Back_Button = "opportunityBackButton";
    public static String accessibilityID_OpportunityVC_Menu_Button = "opportunityMenuButton";
    public static String accessibilityID_OpportunityVC_Banner_Swipe_Right = "right";
    public static String accessibilityID_OpportunityVC_Banner_Swipe_Left = "left";

    // Opportunity Detail
    public static String accessibilityID_OpportunityDetailVC_Back_Button = "icon ustbar back";
    public static String accessibilityID_OpportunityDetailVC_Get_Password_Button = "ŞİFRE AL";
    public static String accessibilityID_Content_Detail_Play_Button = "buttonPlayDetail";
    public static String accessibilityID_Content_Detail_Header  = "icon_app";
    public static String accessibilityID_Content_Detail_Button_Back  = "buttonBackDetail";
    public static String accessibilityID_Player_Done_Button  = "Done";
    public static String accessibilityID_See_More_Button  = "detailCellActionButton";
    public static String accessibilityID_Cancel_ShareSheet_Button  = "Cancel";
    public static String accessibilityID_ForKids_Header  = "ÇOCUĞUM İÇİN";


    // Verification for campaign detail VC
    public static String accessibilityID_Verify_TCKN_textfield = "tcknTextfield";
    public static String accessibilityID_Verify_Birthday_textfield = "birthdayTextfield";
    public static String accessibilityID_Verify_Name_textfield = "nameTextfield";
    public static String accessibilityID_Verify_Surname_textfield = "surnameTextfield";
    public static String accessibilityID_verify_Button = "DOĞRULA";
    public static String accessibilityID_Verify_Birthday_Picker = "birthdayPicker";
    public static String accessibilityID_Verification_Back_Button = "icon ustbar back";
    public static String accessibilityID_Verification_Warning_Popup_OK_Button = "OK";

    // For Kids
    public static String accessibilityID_Filter_Content_Button = "FİLTRELE";
    public static String accessibilityID_Filter_Again_Button = "arrowCopy2";
    public static String accessibilityID_Edit_ChildInfo = "icon edit";

    // Notifications
    public static String accessibilityID_Notifications_Menu_Button = "menuNotifications";

    // User Contract
    public static String accessibilityID_Accep_Tick = "acceptTick";
    public static String accessibilityID_NonTurkcell_Accep_Tick = "nonTurkcellCheckboxTick";
    public static String accessibilityID_Continue_From_Aggreement = "acceptAggreementButton";

    // Toolbar
    public static String accessiblityID_Toolbar_Pohpohla_Button = "pohPohlaButton";
    public static String accessiblityID_Toolbar_EditShare_Button = "icon_bottom_kamera";
    public static String accessiblityID_Toolbar_MakeAlbum_Button = "icon_bottom_album";
    public static String accessiblityID_Toolbar_MyKids_Button = "icon_bottom_cocuklarim";

    // EditShare
    public static String accessiblityID_Edit_Share_Photo_Button = "FOTOĞRAFLARIM";
    public static String accessiblityID_Edit_Share_Contrast = "icon_effect_kontrast_p";
    public static String accessiblityID_Edit_Share_Brightness = "Çerçeve";
    public static String accessiblityID_Edit_Share_Brightness2 = "icon_effect_parlaklik_p";
    public static String accessiblityID_Edit_Share_Filter = "icon_effect_filtre";
    public static String accessiblityID_Edit_Share_Filter2 = "icon_effect_filtre_p";
    public static String accessiblityID_Edit_Share_Frame= "icon_effect_cerceve";
    public static String accessiblityID_Edit_Share_Frame2= "icon_effect_cerceve_p";
    public static String accessiblityID_Edit_Share_Draw= "icon_effect_ciz";
    public static String accessiblityID_Edit_Share_Draw2= "icon_effect_ciz_p";
    public static String accessiblityID_Edit_Share_Cut= "icon_effect_kes";
    public static String accessiblityID_Edit_Share_Cut2= "icon_effect_kes_p";
    public static String accessiblityID_Edit_Share_Caps= "icon_effect_caps";
    public static String accessiblityID_Edit_Share_Caps2= "icon_effect_caps_p";
    public static String accessiblityID_Edit_Share_Blur= "icon_effect_blur";
    public static String accessiblityID_Edit_Share_Blur2= "icon_effect_blur_p";
    public static String accessiblityID_Edit_Share_Text= "icon_effect_text";
    public static String accessiblityID_Edit_Share_Text2= "icon_effect_text_p";
    public static String accessiblityID_Edit_Share_Cancel_Editing= "icon back";
    public static String accessiblityID_Edit_Share_Done_Editing= "icon tick small";
    public static String accessiblityID_Edit_Share_Caps_Textview = "capsTextView";
    public static String accessiblityID_Edit_Share_TextEdit_Textfield = "Metin";
    public static String accessiblityID_Edit_Share_Sticker = "icon_effect_etiket";
    public static String accessiblityID_Edit_Share_Rotate = "icon_effect_dondur";
    public static String accessiblityID_Edit_Sticker_Emocan = "Emocan";
    public static String accessiblityID_Edit_Sticker_Goygoy = "goygoycu";
    public static String accessiblityID_Edit_Sticker_Duzgun = "duzgun";
    public static String accessiblityID_Edit_Sticker_Share_Final = "icon share small";

    // Make Album
    public static String accessibilityID_Make_Album_Header = "ALBÜM YAP";
    public static String accessibilityID_Make_Album_Select_From_CameraRoll = "Kütüphaneden Seç";
    public static String accessibilityID_Make_Album_Add_Text = "YAZI EKLE";
    public static String accessibilityID_Make_Album_Textfield = "addTextFieldAlbum";
    public static String accessibilityID_Make_Album_Textfield_Done = "Done";
    public static String accessibilityID_Make_Album_Turn_Image = "RESMİ ÇEVİR";
    public static String accessibilityID_Make_Album_Continue_Button = "DEVAM ET";
    public static String accessibilityID_Make_Album_Button_Play = "button play";

    // Pohpohla
    public static String accessiblityID_Pohpohla_GetOther_Button = "BAŞKA";
    public static String accessiblityID_Pohpohla_Menu_Button = "leftMenuPohPohButton";

    // Search
    public static String accessibilityID_Search_Header = "icon_app";

    // SSS
    public static String accessibilityID_SSS_Header = "SIKÇA SORULAN SORULAR";

    // Contact Us
    public static String accessibilityID_Send_Button = "GÖNDER";
    public static String accessibilityID_Popup_OK_Button = "OK";
    public static String accessibilityID_Textview = "messageTextviewArea";
    public static String accessibilityID_LeftMenu_Button_ContactUs = "icon ustbar menu";
    public static String accessibilityID_Keyboard_Done_Button = "Tamam";

    // Hesabim
    public static String accessibilityID_Header_Hesabim = "HESABIM";
    public static String accessibilityID_LeftMenu_Hesabim = "balanceMenuButton";
    public static String accessibilityID_InvoiceDetail_button = "DETAYLAR";
    public static String accessibilityID_InvoiceDetail_back_button = "icon ustbar back";

    // Settings
    public static String accessibilityID_Header_Settings = "AYARLAR";
    public static String accessibilityID_PhotoIcon = "overlay_pp_big";
    public static String accessibilityID_Select_Photo_Button = "FOTOĞRAF SEÇ";
    public static String accessibilityID_Camera_Roll = "Camera Roll";
    public static String accessibilityID_Choose_Image = "Choose";
    public static String accessibilityID_Success_Popup_OK = "TAMAM";
    public static String accessibilityID_Menu_Button_Settings = "settingsLeftMenu";
    public static String accessibilityID_Edit_Email_Button = "icon edit";
    public static String accessibilityID_Edit_Email_Textfield = "bg_textbox";
    public static String accessibilityID_Save_Button_Popup = "KAYDET";
    public static String accessibilityID_Cancel_Button_Popup = "VAZGEÇ";
    public static String accessibilityID_Info_Popup_OK_Button = "Tamam";

    // Notifications
    public static String accessibilityID_NotificationsHeader = "BİLDİRİMLER";

    


}
