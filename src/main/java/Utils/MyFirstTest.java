//import com.thoughtworks.selenium.webdriven.commands.Click;
//import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;
//
//import io.appium.java_client.MobileBy;
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.ios.IOSDriver;
//
//import javafx.scene.control.Cell;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.seleniumhq.jetty9.io.MappedByteBufferPool;
//import org.testng.annotations.BeforeClass;
//
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
//import static org.bouncycastle.asn1.x500.style.RFC4519Style.c;
//import static org.bouncycastle.asn1.x500.style.RFC4519Style.name;
//import static org.openqa.selenium.By.name;
//import static org.openqa.selenium.By.tagName;
//import static org.testng.TestNGAntTask.Mode.testng;
//
//
//public class MyFirstTest {
//
//
//    IOSDriver driver;
//    WebDriverWait driverWait;
//    @Before
//    public void setup() throws MalformedURLException {
//
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability("platformName", iOSConstants.PLATFORM_NAME);
//        capabilities.setCapability("deviceName", iOSConstants.DEVICE_NAME);
//        capabilities.setCapability("deviceType", iOSConstants.DEVICE_TYPE);
//        capabilities.setCapability("bundleId", iOSConstants.BUNDLE_ID);
//        capabilities.setCapability("automationName", iOSConstants.AUTOMATION_NAME);
//        capabilities.setCapability("clearSystemFiles", true);
//        capabilities.setCapability("platformVersion", iOSConstants.PLATFORM_VERSION);
//        capabilities.setCapability("udid", iOSConstants.UDID);
//        driver = new IOSDriver(new URL(iOSConstants.URL), capabilities);
//
//        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//        driverWait = new WebDriverWait(driver, 1000);
//    }
//
//    @After
//    public void tearDown() {
////        driver.quit();
//    }
//
//    @Test
//    public void Login() {
//        // login splash
//        driver.findElementByAccessibilityId("loginSplashLogin").click();
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//
//        // login
//        MobileElement phoneTextfield = (MobileElement) driver.findElementByAccessibilityId("phoneTextField");
//        phoneTextfield.sendKeys(iOSConstants.MSISDN);
//        MobileElement passwordTextfield = (MobileElement) driver.findElementByAccessibilityId("passwordTextField");
//        passwordTextfield.sendKeys(iOSConstants.PASSWORD);
//        driver.findElementByAccessibilityId("tapToLogin").click();
//
//        // pohpohla from home
//        driverWait.until(ExpectedConditions.presenceOfElementLocated(MobileBy.ByAccessibilityId.id("pohPohlaButton")));
//      //  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        driver.findElementByAccessibilityId("pohPohlaButton").click();
//
//
//        // go hesabim from pohpohla
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.findElementByAccessibilityId("leftMenuPohPohButton").click();
//        MobileElement leftMenuTableView = (MobileElement) driver.findElementByAccessibilityId("leftMenuList");
//        MobileElement el3 = (MobileElement) driver.findElementByXPath("(//XCUIElementTypeCell[@name=\"leftMenuCell\"])[4]");
//        el3.click();
//
//        // go cocuklarim from hesabim
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        driver.findElementByAccessibilityId("balanceTabbarMyKidsButton").click();
//
//        // go children list from cocuklarim
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        MobileElement childList = (MobileElement) driver.findElementByAccessibilityId("childrenListTableView");
//        MobileElement childCell = childList.findElementsByAccessibilityId("childCell").get(0);
//        childCell.click();
//
//        // go enter child data from children list
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.findElementByAccessibilityId("addNewEntryForKidButton").click();
//
//        MobileElement heightInfoTextfield = (MobileElement) driver.findElementByAccessibilityId("heightInfo");
//        heightInfoTextfield.sendKeys("111");
//        MobileElement weightInfoTextfield = (MobileElement) driver.findElementByAccessibilityId("weightInfo");
//        weightInfoTextfield.sendKeys("11");
//        driver.findElementByAccessibilityId("updateChildInfo").click();
//
//        // go child data summamry from child profile
//        driverWait.until(ExpectedConditions.presenceOfElementLocated(MobileBy.ByAccessibilityId.id("gelisimKarnesiButton")));
//        driver.findElementByAccessibilityId("gelisimKarnesiButton").click();
//
//
//    }
//
//}
