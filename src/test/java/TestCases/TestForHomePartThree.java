package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForHomePartThree {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "check home for home button failed");
    }

    @Test(dependsOnMethods = {"checkHomeForHomeButton"})
    public void clickHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "click home for home button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForHomeButton"})
    public void checkForHomeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for home gift button failed");
    }

    @Test(dependsOnMethods = {"checkForHomeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(205,642).moveTo(13,-443).release().perform();
        (new TouchAction(driver)).press(220,181).moveTo(-11,447).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDownAndUp"})
    public void clickDecorations () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(174, 633).perform();
    }
    @Test(dependsOnMethods = {"clickDecorations"})
    public void checkHeaderContent() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check header failed");
    }

    @Test(dependsOnMethods = {"checkHeaderContent"})
    public void scrollUpAndDown() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(236,657).moveTo(5,-442).release().perform();
        (new TouchAction(driver)).press(236,137).moveTo(17,481).release().perform();
    }

    @Test(dependsOnMethods = {"scrollUpAndDown"})
    public void clickFirstContentOfDetailList() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(223, 116).perform();
    }

    @Test(dependsOnMethods = {"clickFirstContentOfDetailList"})
    public void checkHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check header failed");
    }
    @Test(dependsOnMethods = {"checkHeader"})
    public void scrollDownToShare() throws MalformedURLException {
        (new TouchAction(driver)).press(171, 670).moveTo(21, -487).release().perform();
        (new TouchAction(driver)).press(161, 677).moveTo(2, -517).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDownToShare"})
    public void checkShareButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "Click share button failed");
    }

    @Test(dependsOnMethods = {"checkShareButton"})
    public void clickShareButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "Click share button failed");
    }

    @Test(dependsOnMethods = {"clickShareButton"})
    public void checkCancelShareSheetButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Cancel_ShareSheet_Button,
                "Click share sheet button failed");
    }

    @Test(dependsOnMethods = {"checkCancelShareSheetButton"})
    public void clickCancelShareSheetButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Cancel_ShareSheet_Button,
                "Cancel share sheet button failed");
    }
    @Test(dependsOnMethods = {"clickCancelShareSheetButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check detail back button failed");
    }

    @Test(dependsOnMethods = {"checkDetailBackButton"})
    public void clickDetailBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click detail back button failed");
    }


}
