package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForMePartTwo {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "check home for me button failed");
    }



    @Test(dependsOnMethods = {"checkHomeForMeButton"})
    public void clickHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "click home for me button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForMeButton"})
    public void checkForMeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for me gift button failed");
    }

    @Test(dependsOnMethods = {"checkForMeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");
    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");
    }

    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollUpAndDown() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(271, 605).moveTo(-5, -408).release().perform();
    }

    @Test(dependsOnMethods = {"scrollUpAndDown"})
    public void clickFourthContentOfDetailList() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(179, 659).perform();
    }

    @Test(dependsOnMethods = {"clickFourthContentOfDetailList"})
    public void checkHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check header failed");
    }

    @Test(dependsOnMethods = {"checkHeader"})
    public void clickFirstContent() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(212, 132).perform();
    }

    @Test(dependsOnMethods = {"clickFourthContentOfDetailList"})
    public void checkDetailContentHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check content detail header failed");
    }

    @Test(dependsOnMethods = {"checkDetailContentHeader"})
    public void scrollDownToSeeMoreButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(248,667).moveTo(1, -277).release().perform();
        (new TouchAction(driver)).press(249, 704).moveTo(14,-515).release().perform();
        (new TouchAction(driver)).press(249, 704).moveTo(7,-544).release().perform();
        (new TouchAction(driver)).press(287, 686).moveTo(6, -507).release().perform();
        (new TouchAction(driver)).press(261, 688).moveTo(-18, -549).release().perform();
        (new TouchAction(driver)).press(207, 655).moveTo(-11, -516).release().perform();
        (new TouchAction(driver)).press(103, 678).moveTo(55, -474).release().perform();
        (new TouchAction(driver)).press(245, 722).moveTo(-43,-606).release().perform();
    }
    @Test(dependsOnMethods = {"scrollDownToSeeMoreButton"})
    public void clickSeeMoreButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "See More button failed");
    }

    @Test(dependsOnMethods = {"clickSeeMoreButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check detail back button failed");
    }

    @Test(dependsOnMethods = {"checkDetailBackButton"})
    public void clickDetailBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click detail back button failed");
    }
}
