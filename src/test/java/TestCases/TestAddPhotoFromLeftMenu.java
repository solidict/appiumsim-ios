package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestAddPhotoFromLeftMenu {
    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "check home menu button");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "click home left menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void clickHPhotoOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(62, 101).perform();
    }

    @Test(dependsOnMethods = {"clickHPhotoOnLeftMenu"})
    public void clickSelectFromLibrary() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_SelectFromLibrary,
                "click select from library failed");
    }

    @Test(dependsOnMethods = {"clickSelectFromLibrary"})
    public void selectPhotoFromLibrary() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(139, 215).perform();
        (new TouchAction(driver)).tap(42, 132).perform();
        (new TouchAction(driver)).tap(328, 770).perform();
    }

    @Test(dependsOnMethods = {"selectPhotoFromLibrary"})
    public void checkOKOnPopup() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "check OK on pop up failed");
    }

    @Test(dependsOnMethods = {"checkOKOnPopup"})
    public void clickOKOnPopup() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click OK on pop up failed");
    }

}
