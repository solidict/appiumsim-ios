package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestPortOutUser {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Test(dependsOnMethods = {"setup"})
    public void checkSplashLoginButton() throws MalformedURLException {
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_LoginSplash_Login_Button, "login splash cannot found");
    }

    @Test(dependsOnMethods = {"checkSplashLoginButton"})
    public void clickSplashLoginButton() throws MalformedURLException {
        //Click Giris Yap Button
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LoginSplash_Login_Button, "Splash login button not clicked");

    }

    @Test(dependsOnMethods = {"clickSplashLoginButton"})
    public void typeGSMNumber() throws MalformedURLException {

        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textField_GSMNumber, iOSConstants.MSISDN, "textfield GSM not found" );
    }

    @Test(dependsOnMethods = {"typeGSMNumber"})
    public void typePassword() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textField_GSMPassword, iOSConstants.PASSWORD, "textfield password not found" );
    }

    @Test(dependsOnMethods = {"typePassword"})
    public void checkLoginButton() throws MalformedURLException {

        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Login_Bg_Splash_JPG, "click empty area failed!");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_Login_LoginButton, "Login button cannot found");
    }


    @Test(dependsOnMethods = {"checkLoginButton"})
    public void clickLoginButton() throws MalformedURLException {

        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Login_Bg_Splash_JPG, "click empty area failed!");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Login_LoginButton, "Login click button on login page failed");
    }

    @Test(dependsOnMethods = {"clickLoginButton"})
    public void typeName() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textFieldName,
                iOSConstants.SIGNUP_NAME,
                "textfield name not found" );
    }
    @Test(dependsOnMethods = {"typeName"})
    public void typeSurname() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textFieldSurname,
                iOSConstants.SIGNUP_SURNAME,
                "textfield surname not found" );
    }
    @Test(dependsOnMethods = {"typeSurname"})
    public void clickBirthdayField() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_textFieldBirthday,
                "click birthday textfield failed");
    }
    @Test(dependsOnMethods = {"clickBirthdayField"})
    public void clickDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done button failed");
    }
    @Test(dependsOnMethods = {"clickDoneButton"})
    public void clickMaleButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_selectMaleButton,
                "click male button failed");
    }
    @Test(dependsOnMethods = {"clickMaleButton"})
    public void clickContinueButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continueButton,
                "click continue button failed");
    }
    @Test(dependsOnMethods = {"clickContinueButton"})
    public void checkHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Signup_Login_Header,
                "header cannot found");
    }
}
