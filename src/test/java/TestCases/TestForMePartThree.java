package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForMePartThree {


    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup() {
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }

    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "check home for me button failed");
    }


    @Test(dependsOnMethods = {"checkHomeForMeButton"})
    public void clickHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "click home for me button failed");
    }

    @Test(dependsOnMethods = {"clickHomeForMeButton"})
    public void checkForMeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for me gift button failed");
    }

    @Test(dependsOnMethods = {"checkForMeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }

    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDownAndSelectContent() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(266, 582).moveTo(0, -305).release().perform();
        (new TouchAction(driver)).press(194, 685).moveTo(-5, -426).release().perform();
        (new TouchAction(driver)).press(173, 620).moveTo(-16, -411).release().perform();
        (new TouchAction(driver)).press(101, 574).moveTo(29, -362).release().perform();
        (new TouchAction(driver)).press(222, 587).moveTo(19, -324).release().perform();
        (new TouchAction(driver)).tap(215, 655).perform();
    }

    @Test(dependsOnMethods = {"scrollDownAndSelectContent"})
    public void checkDetailContentHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check content detail header failed");
    }

    @Test(dependsOnMethods = {"checkDetailContentHeader"})
    public void clickFirstContent() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(189, 396).perform();

    }

    @Test(dependsOnMethods = {"clickFirstContent"})
    public void scrollDownToShare() throws MalformedURLException {
        (new TouchAction(driver)).press(171, 670).moveTo(21, -487).release().perform();
        (new TouchAction(driver)).press(161, 677).moveTo(2, -517).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDownToShare"})
    public void checkShareButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "Click share button failed");
    }

    @Test(dependsOnMethods = {"checkShareButton"})
    public void clickShareButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "Click share button failed");
    }

    @Test(dependsOnMethods = {"clickShareButton"})
    public void checkCancelShareSheetButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Cancel_ShareSheet_Button,
                "Click share sheet button failed");
    }

    @Test(dependsOnMethods = {"checkCancelShareSheetButton"})
    public void clickCancelShareSheetButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Cancel_ShareSheet_Button,
                "Cancel share sheet button failed");
    }

}
