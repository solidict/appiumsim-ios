package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForMePartOne {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "check home for me button failed");
    }



    @Test(dependsOnMethods = {"checkHomeForMeButton"})
    public void clickHomeForMeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForMe_Button,
                "click home for me button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForMeButton"})
    public void checkForMeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for me gift button failed");
    }

    @Test(dependsOnMethods = {"checkForMeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }

    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollUpAndDown() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press( 315, 585).moveTo(-13,-344).release()
                .perform();

        (new TouchAction(driver))
                .press(308, 174).moveTo(-13, 367).release().perform();
    }

    @Test(dependsOnMethods = {"scrollUpAndDown"})
    public void clickFirstContentOfDetailList() throws MalformedURLException {
        (new TouchAction(driver)).tap(176, 354).perform();
    }

    @Test(dependsOnMethods = {"clickFirstContentOfDetailList"})
    public void checkContentListHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "check content list header failed");
    }

    @Test(dependsOnMethods = {"checkContentListHeader"})
    public void clickFirstContentOfDetailListing() throws MalformedURLException {
        (new TouchAction(driver)).tap(108, 225).perform();
    }

    @Test(dependsOnMethods = {"clickFirstContentOfDetailListing"})
    public void checkContentDetailHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "check content detail header failed");
    }

    @Test(dependsOnMethods = {"checkContentDetailHeader"})
    public void checkPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "check play button failed");
    }

    @Test(dependsOnMethods = {"checkPlayButton"})
    public void clickPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "click play button failed");
    }

    @Test(dependsOnMethods = {"clickPlayButton"})
    public void checkPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "check player done button failed");
    }

    @Test(dependsOnMethods = {"checkPlayerDoneButton"})
    public void clickPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "click player done button failed");
    }

    @Test(dependsOnMethods = {"clickPlayerDoneButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check content back button failed");
    }
    @Test(dependsOnMethods = {"checkContentBackButton"})
    public void clickContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click content back button failed");
    }

}
