package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.jboss.netty.util.ExternalResourceUtil.release;

public class TestForHomePartTwo {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "check home for home button failed");
    }

    @Test(dependsOnMethods = {"checkHomeForHomeButton"})
    public void clickHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "click home for home button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForHomeButton"})
    public void checkForHomeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for home gift button failed");
    }

    @Test(dependsOnMethods = {"checkForHomeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDown() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(232,652).moveTo(17,-484).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDown"})
    public void clickDecorations () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(188, 266).perform();
    }

    @Test(dependsOnMethods = {"clickDecorations"})
    public void scrollDownDetail() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(218,584).moveTo(15,-416).release().perform();
        (new TouchAction(driver)).press(218,598).moveTo(4,-349).release().perform() ;
    }

    @Test(dependsOnMethods = {"scrollDownDetail"})
    public void clickDecorationDetails () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(202, 220).perform();
    }

    @Test(dependsOnMethods = {"clickDecorationDetails"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check content back button failed");
    }
    @Test(dependsOnMethods = {"checkContentBackButton"})
    public void clickContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click content back button failed");
    }

}
