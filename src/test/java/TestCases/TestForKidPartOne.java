package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.jboss.netty.util.ExternalResourceUtil.release;

public class TestForKidPartOne {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkKidGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkKidGiftBoxButton"})
    public void checkKidForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "check home for kids button failed");
    }

    @Test(dependsOnMethods = {"checkKidForHomeButton"})
    public void clickHomeForKidButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "click home for kid button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForKidButton"})
    public void checkForKidBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check for kid back button failed");
    }

    @Test(dependsOnMethods = {"checkForKidBackButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDown() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(284,639).moveTo(-36,-474).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDown"})
    public void clickSelectedContent () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(201, 290).perform();
    }

    @Test(dependsOnMethods = {"clickSelectedContent"})
    public void checkHeaderContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "Check header content back button failed");
    }

    @Test(dependsOnMethods = {"checkHeaderContentBackButton"})
    public void scrollUpAndDown() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(245,598).moveTo(24,-396).release().perform();
        (new TouchAction(driver)).press(274,117).moveTo(5, 511).release().perform();
    }

    @Test(dependsOnMethods = {"scrollUpAndDown"})
    public void clickFirstContentOfDetailList() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(209, 170).perform();
    }

    @Test(dependsOnMethods = {"clickFirstContentOfDetailList"})
    public void checkContentDetailsBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "Check content detail back button failed");
    }

    @Test(dependsOnMethods = {"checkContentDetailsBackButton"})
    public void scrollKidContentDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(184,668).moveTo(-5,-476).release().perform();
        (new TouchAction(driver)).press(207,181).moveTo(-6,427).release().perform();
    }

    @Test(dependsOnMethods = {"scrollKidContentDownAndUp"})
    public void clickKidContentDetailContent () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(163, 147).perform();
    }

    @Test(dependsOnMethods = {"clickKidContentDetailContent"})
    public void checkContentDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "check content detail button back  failed");
    }

    @Test(dependsOnMethods = {"checkContentDetailBackButton"})
    public void checkPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "check play button failed");
    }

    @Test(dependsOnMethods = {"checkPlayButton"})
    public void clickPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "click play button failed");
    }

    @Test(dependsOnMethods = {"clickPlayButton"})
    public void checkPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "check player done button failed");
    }

    @Test(dependsOnMethods = {"checkPlayerDoneButton"})
    public void clickPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "click player done button failed");
    }

    @Test(dependsOnMethods = {"clickPlayerDoneButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check content back button failed");
    }
    @Test(dependsOnMethods = {"checkContentBackButton"})
    public void clickContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click content back button failed");
    }
}
