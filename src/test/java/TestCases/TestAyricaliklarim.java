package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestAyricaliklarim {


    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "click home menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void clickOpportunitiesButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LeftMenu_Opportunities, "Click on opportunities failed");

    }

    @Test(dependsOnMethods = {"clickOpportunitiesButton"})
        public void checkLeftMenuButtonOnOpportunityVC() throws MalformedURLException {

            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            appiumUtils.checkElement(driver, SimIOSData.accessibilityID_OpportunityVC_Menu_Button, "left menu button cannot found");
        }

        @Test(dependsOnMethods = {"checkLeftMenuButtonOnOpportunityVC"})
        public void clickLeftMenuButtonOnOpportunityVC() throws MalformedURLException {

            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            appiumUtils.clickElement(driver, SimIOSData.accessibilityID_OpportunityVC_Menu_Button, "left menu button cannot clicked");

        }

        @Test(dependsOnMethods = {"clickLeftMenuButtonOnOpportunityVC"})
        public void clickLeftMenuHomeButton() throws MalformedURLException {

            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LeftMenu_Home, "opportunities menu button cannot found");
    }



}
