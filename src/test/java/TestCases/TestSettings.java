package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestSettings {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "check home menu button");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "click home left menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void clickSettingsOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Settings,
                "Click settings on left menu failed");

    }

    @Test(dependsOnMethods = {"clickSettingsOnLeftMenu"})
    public void checkHeaderOnSettings() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Header_Settings,
                "check header on settings failed.");
    }

    @Test(dependsOnMethods = {"checkHeaderOnSettings"})
    public void clickProfileImage() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_PhotoIcon,
                "click profile image failed.");

    }

    @Test(dependsOnMethods = {"clickProfileImage"})
    public void clickChoosePhotoFromCameraRoll() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Select_Photo_Button,
                "click chose photo from camera roll failed.");

    }

    @Test(dependsOnMethods = {"clickChoosePhotoFromCameraRoll"})
    public void clickCameraRoll () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Camera_Roll,
                "click camera roll failed.");

    }

    @Test(dependsOnMethods = {"clickCameraRoll"})
    public void chooseImageFromCameraroll() throws MalformedURLException {

        new TouchAction(driver).tap(48, 140).perform();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Choose_Image,
                "choose image failed.");
    }

    @Test(dependsOnMethods = {"chooseImageFromCameraroll"})
    public void checkSuccessPopupOKButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Success_Popup_OK,
                "success pop up OK button cannot found");
    }

    @Test(dependsOnMethods = {"checkSuccessPopupOKButton"})
    public void clickSuccessPopupOKButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Success_Popup_OK,
                "click success pop up OK button failed");
    }

    @Test(dependsOnMethods = {"clickSuccessPopupOKButton"})
    public void checkEditEmailButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Edit_Email_Button,
                "check   edit email button failed");
    }

    @Test(dependsOnMethods = {"checkEditEmailButton"})
    public void clickEditEmailButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Edit_Email_Button,
                "click edit email button failed");
    }

    @Test(dependsOnMethods = {"clickEditEmailButton"})
    public void clickCancelPopup() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Cancel_Button_Popup,
                "click cancel popup button failed");
    }

    @Test(dependsOnMethods = {"clickCancelPopup"})
    public void clickEditEmailButtonAgain() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Edit_Email_Button,
                "click edit email button again failed");
    }

    @Test(dependsOnMethods = {"clickEditEmailButtonAgain"})
    public void typeNewEmail() throws MalformedURLException {

        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Edit_Email_Textfield,
                iOSConstants.TEST_EMAIL,
                "textfield email not found");
    }

    @Test(dependsOnMethods = {"typeNewEmail"})
    public void checkSaveButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Save_Button_Popup,
                "check save button failed");
    }
    @Test(dependsOnMethods = {"checkSaveButton"})
    public void clickSaveButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Save_Button_Popup,
                "click save button failed");
    }

    @Test(dependsOnMethods = {"clickSaveButton"})
    public void clickInfoPopupOKButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Info_Popup_OK_Button,
                "click info popup ok button failed");
    }

    @Test(dependsOnMethods = {"clickInfoPopupOKButton"})
    public void checkLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Menu_Button_Settings,
                "check left menu button on settings vc failed");
    }

    @Test(dependsOnMethods = {"checkLeftMenuButton"})
    public void clickLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Menu_Button_Settings,
                "click left menu button on settings vc failed");
    }

    @Test(dependsOnMethods = {"clickLeftMenuButton"})
    public void clickHomeButtonOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "click HomeButton on left menu failed");
    }
}
