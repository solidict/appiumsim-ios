package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForKidPartTwo {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkKidGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkKidGiftBoxButton"})
    public void checkKidForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "check home for kids button failed");
    }

    @Test(dependsOnMethods = {"checkKidForHomeButton"})
    public void clickHomeForKidButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "click home for kid button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForKidButton"})
    public void checkForKidBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check for kid back button failed");
    }

    @Test(dependsOnMethods = {"checkForKidBackButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDown() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(245,662).moveTo(18,-543).release().perform();
        (new TouchAction(driver)).press(202,642).moveTo(-16,-499).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDown"})
    public void clickSelectedContent () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(179, 413).perform();
    }

    @Test(dependsOnMethods = {"clickSelectedContent"})
    public void checkHeaderContent() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check for kids header failed");
    }

    @Test(dependsOnMethods = {"checkHeaderContent"})
    public void scrollUpAndDown() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(210,701).moveTo(-13,-544).release().perform();
        (new TouchAction(driver)).press(179,652).moveTo(18,-500).release().perform();
        (new TouchAction(driver)).press(199,706).moveTo(11,-540).release().perform();
        (new TouchAction(driver)).press(194,660).moveTo(2,-494).release().perform();
        (new TouchAction(driver)).press(230,695).moveTo(-13,-550).release().perform();
    }

    @Test(dependsOnMethods = {"scrollUpAndDown"})
    public void clickFirstContent() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(160, 473).perform();
    }

    @Test(dependsOnMethods = {"clickFirstContent"})
    public void checkDetailContentHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check content detail header failed");
    }

    @Test(dependsOnMethods = {"checkDetailContentHeader"})
    public void clickSeeMoreButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_See_More_Button,
                "See More button failed");
    }

    @Test(dependsOnMethods = {"clickSeeMoreButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check detail back button failed");
    }

    @Test(dependsOnMethods = {"checkDetailBackButton"})
    public void clickDetailBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click detail back button failed");
    }

}
