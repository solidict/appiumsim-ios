package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestHelp {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "click home menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void checkLeftMenuHelpButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Help,
                "Help button on left menu cannot found");
    }
    @Test(dependsOnMethods = {"checkLeftMenuHelpButton"})
    public void clickLeftMenuHelpButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Help,
                "Click Help button on left menu failed");
    }

    @Test(dependsOnMethods = {"clickLeftMenuHelpButton"})
    public void checkHomeMenuButton2() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }


}
