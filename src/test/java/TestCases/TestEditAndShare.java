package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.jboss.netty.util.ExternalResourceUtil.release;

public class TestEditAndShare {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void checkToolbarEditShareButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_EditShare_Button,
                " pohpohla button cannot found");
    }

    @Test(dependsOnMethods = {"checkToolbarEditShareButton"})
    public void clickToolbarEditShareButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_EditShare_Button,
                "click Toolbar edit share button failed");
    }

    @Test(dependsOnMethods = {"clickToolbarEditShareButton"})
    public void checkEditSharePhotosButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Photo_Button,
                "check edit share photo button failed");
    }
    @Test(dependsOnMethods = {"clickToolbarEditShareButton"})
    public void clickEditSharePhotosButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Photo_Button,
                "click edit share photo button failed");
    }

    @Test(dependsOnMethods = {"clickEditSharePhotosButton"})
    public void clickSelectImagesButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(186, 649).perform();
    }

    @Test(dependsOnMethods = {"clickSelectImagesButton"})
    public void clickContrastButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Contrast,
                "click contrast button failed");
    }

    @Test(dependsOnMethods = {"clickContrastButton"})
    public void changeImageContrast() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"changeImageContrast"})
    public void clickCancelContrastChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel contrast changes contrast failed");
    }
    @Test(dependsOnMethods = {"clickCancelContrastChanges"})
    public void clickContrastEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Contrast,
                "click contrast edit button failed");
    }

    @Test(dependsOnMethods = {"clickContrastEditButton"})
    public void changeImageContrastSettings() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"clickCancelContrastChanges"})
    public void clickContrastEditDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click contrast edit done button failed");
    }
    @Test(dependsOnMethods = {"clickContrastEditDoneButton"})
    public void clickBrightnessButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Brightness,
                "click brightness button failed");
    }

    @Test(dependsOnMethods = {"clickBrightnessButton"})
    public void changeImageBrightness() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(189, 765).moveTo(-83, 0).release();
    }

    @Test(dependsOnMethods = {"changeImageBrightness"})
    public void clickCancelBrightnessChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel birghtness changes contrast failed");
    }

    @Test(dependsOnMethods = {"clickCancelBrightnessChanges"})
    public void checkBrightnessEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Brightness,
                "check brightness edit button failed");
    }
    @Test(dependsOnMethods = {"checkBrightnessEditButton"})
    public void clickBrightnessEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Brightness,
                "click brightness edit button failed");
    }

    @Test(dependsOnMethods = {"clickBrightnessEditButton"})
    public void changeImageBrightnessSettings() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(189, 765).moveTo(-83, 0).release();
    }

    @Test(dependsOnMethods = {"changeImageBrightnessSettings"})
    public void clickBrightnessEditDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click brightness edit done button failed");
    }
    @Test(dependsOnMethods = {"clickBrightnessEditDoneButton"})
    public void clickFilterButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Filter,
                "click filter button failed");
    }
    @Test(dependsOnMethods = {"clickFilterButton"})
    public void scrollFilters() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(312,770).moveTo(-194,5).release().perform();
        (new TouchAction(driver)).press(76,766).moveTo(238,4).release().perform();
    }

    @Test(dependsOnMethods = {"scrollFilters"})
    public void selectSecondFilter() throws MalformedURLException {
        (new TouchAction(driver)).tap(124, 771).perform();
    }
    @Test(dependsOnMethods = {"selectSecondFilter"})
    public void clickCancelFilterChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel filter changes filter failed");
    }

    @Test(dependsOnMethods = {"clickCancelFilterChanges"})
    public void clickFilterEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Filter,
                "click filter edit button failed");
    }
    @Test(dependsOnMethods = {"clickFilterEditButton"})
    public void scrollEditFilters() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(312,770).moveTo(-194,5).release().perform();
        (new TouchAction(driver)).press(76,766).moveTo(238,4).release().perform();
    }

    @Test(dependsOnMethods = {"scrollEditFilters"})
    public void selectSecondFilterEdit() throws MalformedURLException {
        (new TouchAction(driver)).tap(124, 771).perform();
    }
    @Test(dependsOnMethods = {"selectSecondFilterEdit"})
    public void clickFilterDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click filter button failed");
    }

    @Test(dependsOnMethods = {"clickFilterDoneButton"})
    public void clickFrameButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Frame,
                "click frame button failed");
    }
    @Test(dependsOnMethods = {"clickFrameButton"})
    public void scrollFrames() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(312,770).moveTo(-194,5).release().perform();
        (new TouchAction(driver)).press(76,766).moveTo(238,4).release().perform();
    }

    @Test(dependsOnMethods = {"scrollFrames"})
    public void selectSecondFrame() throws MalformedURLException {
        (new TouchAction(driver)).tap(124, 771).perform();
    }
    @Test(dependsOnMethods = {"selectSecondFrame"})
    public void clickCancelFrameChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel frame changes filter failed");
    }

    @Test(dependsOnMethods = {"clickCancelFrameChanges"})
    public void clickFrameEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Frame,
                "click frame edit button failed");
    }
    @Test(dependsOnMethods = {"clickFrameEditButton"})
    public void scrollEditFrames() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(312,770).moveTo(-194,5).release().perform();
        (new TouchAction(driver)).press(76,766).moveTo(238,4).release().perform();
    }

    @Test(dependsOnMethods = {"scrollEditFrames"})
    public void selectSecondFrameEdit() throws MalformedURLException {
        (new TouchAction(driver)).tap(124, 771).perform();
    }
    @Test(dependsOnMethods = {"selectSecondFrameEdit"})
    public void clickFrameDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click frame done button failed");
    }

    @Test(dependsOnMethods = {"clickFrameDoneButton"})
    public void clickDrawingEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Draw,
                "click drawing editing button failed");
    }
    @Test(dependsOnMethods = {"clickDrawingEditing"})
    public void clickSelectRedColorEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(111, 748).perform();
    }
    @Test(dependsOnMethods = {"clickSelectRedColorEditing"})
    public void clickThickestPenEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(318, 770).perform();
    }
    @Test(dependsOnMethods = {"clickThickestPenEditing"})
    public void drawditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(57,514).moveTo(207,-290).release().perform();
    }
    @Test(dependsOnMethods = {"selectSecondFrame"})
    public void clickCancelDrawingChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel drawing changes filter failed");
    }

    @Test(dependsOnMethods = {"clickCancelDrawingChanges"})
    public void clickDrawingImageEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Draw2,
                "click drawing image editing button failed");
    }
    @Test(dependsOnMethods = {"clickDrawingImageEditing"})
    public void clickSelectDrawingRedColorEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(111, 748).perform();
    }
    @Test(dependsOnMethods = {"clickSelectDrawingRedColorEditing"})
    public void clickDrawingThickestPenEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(318, 770).perform();
    }
    @Test(dependsOnMethods = {"clickDrawingThickestPenEditing"})
    public void drawEditingImage() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(57,514).moveTo(207,-290).release().perform();
    }
    @Test(dependsOnMethods = {"drawEditingImage"})
    public void clickDrawEditingDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click draw editing done button failed");
    }
    @Test(dependsOnMethods = {"clickDrawEditingDoneButton"})
    public void scrollEditMenuToRight() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(308,769).moveTo(-194,2).release().perform();
    }
    @Test(dependsOnMethods = {"scrollEditMenuToRight"})
    public void clickCutEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cut,
                "click cut editing button failed");
    }
    @Test(dependsOnMethods = {"clickCutEditing"})
    public void cutSelectedImageEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(359,94).moveTo(-108,202).release().perform();
    }
    @Test(dependsOnMethods = {"cutSelectedImageEditing"})
    public void clickCancelCutEditingChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel cut editing changes filter failed");
    }
    @Test(dependsOnMethods = {"clickCancelCutEditingChanges"})
    public void clickCutImageEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cut,
                "click cut image editing button failed");
    }
    @Test(dependsOnMethods = {"clickCutImageEditing"})
    public void cutSelectedImageEditingEdit() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(359,94).moveTo(-108,202).release().perform();
    }
    @Test(dependsOnMethods = {"cutSelectedImageEditingEdit"})
    public void clickDrawEditingImageDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click draw editing image done button failed");
    }

    @Test(dependsOnMethods = {"clickDrawEditingImageDoneButton"})
    public void clickCapsEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Caps,
                "click caps editing button failed");
    }
    @Test(dependsOnMethods = {"clickCapsEditing"})
    public void typeCapsText() throws MalformedURLException {
        appiumUtils.typeData(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Caps_Textview,
                iOSConstants.CAPS_TEXT,
                "caps textview not found" );
    }
    @Test(dependsOnMethods = {"typeCapsText"})
    public void clickCancelCapsEditingChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel caps editing changes filter failed");
    }
    @Test(dependsOnMethods = {"clickCancelCapsEditingChanges"})
    public void clickCapsImageEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Caps                                                                                                                                                                                                                     ,
                "click caps image editing button failed");
    }
    @Test(dependsOnMethods = {"clickCapsImageEditing"})
    public void typeCapsTextImage() throws MalformedURLException {
        appiumUtils.typeData(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Caps_Textview,
                iOSConstants.CAPS_TEXT,
                "caps text image not found" );
    }
    @Test(dependsOnMethods = {"typeCapsTextImage"})
    public void clickCapsEditingImageDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click caps editing image done button failed");
    }

    @Test(dependsOnMethods = {"clickCapsEditingImageDoneButton"})
    public void clickBlurButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Blur,
                "click blur button failed");
    }

    @Test(dependsOnMethods = {"clickBlurButton"})
    public void changeImageBlur() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"changeImageBlur"})
    public void clickCancelBlurChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel blur changes contrast failed");
    }
    @Test(dependsOnMethods = {"clickCancelBlurChanges"})
    public void clickBlurEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Blur2,
                "click blur edit button failed");
    }

    @Test(dependsOnMethods = {"clickBlurEditButton"})
    public void changeImageBlurSettings() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"changeImageBlurSettings"})
    public void clickBlurEditDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click blur edit done button failed");
    }

    @Test(dependsOnMethods = {"clickBlurEditDoneButton"})
    public void clickAddTextButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Text,
                "click edit text button failed");
    }

    @Test(dependsOnMethods = {"clickAddTextButton"})
    public void typeEditTextImage() throws MalformedURLException {
        appiumUtils.typeData(
                driver,
                SimIOSData.accessiblityID_Edit_Share_TextEdit_Textfield,
                iOSConstants.TEXT_EDIT_TEXT,
                "edit text not found");
    }

    @Test(dependsOnMethods = {"typeEditTextImage"})
    public void clickCancelTextEditChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel text edit changes contrast failed");
    }

    @Test(dependsOnMethods = {"clickCancelTextEditChanges"})
    public void clickAddTextImageButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Text2,
                "click add text image  button failed");
    }

    @Test(dependsOnMethods = {"clickAddTextImageButton"})
    public void typeEditTextImageEdit() throws MalformedURLException {
        appiumUtils.typeData(
                driver,
                SimIOSData.accessiblityID_Edit_Share_TextEdit_Textfield,
                iOSConstants.TEXT_EDIT_TEXT,
                "edit text not found");
    }
    @Test(dependsOnMethods = {"typeEditTextImageEdit"})
    public void clickTextEditDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click text edit done button failed");
    }
    @Test(dependsOnMethods = {"clickTextEditDoneButton"})
    public void scrollToRightToEdit() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(311,770).moveTo(-210, 11).release().perform();
    }
    @Test(dependsOnMethods = {"scrollToRightToEdit"})
    public void clickStickerEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Sticker,
                "click sticker edit button failed");
    }
    @Test(dependsOnMethods = {"clickStickerEditButton"})
    public void clickStickerEmocan() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Sticker_Emocan,
                "click sticker emocan");
    }
    @Test(dependsOnMethods = {"clickStickerEmocan"})
    public void scrollStickerDownAndUp() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(329,590).moveTo(9,-352).release().perform();
        (new TouchAction(driver)).press(336,161).moveTo(-7,315).release().perform();
    }
    @Test(dependsOnMethods = {"scrollStickerDownAndUp"})
    public void selectThirdContent() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Sticker_Duzgun,
                "click third content");
    }
    @Test(dependsOnMethods = {"selectThirdContent"})
    public void clickStickerCancelSelected() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(184, 236).perform();
    }


    @Test(dependsOnMethods = {"clickStickerCancelSelected"})
    public void clickStickerEmocanEditing() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Sticker_Emocan,
                "click sticker emocan editing");
    }
    @Test(dependsOnMethods = {"clickStickerEmocanEditing"})
    public void scrollStickerEditDownAndUp() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(329,590).moveTo(9,-352).release().perform();
        (new TouchAction(driver)).press(336,161).moveTo(-7,315).release().perform();
    }
    @Test(dependsOnMethods = {"scrollStickerEditDownAndUp"})
    public void selectFourthContent() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Sticker_Goygoy,
                "click fourth content");
    }
    @Test(dependsOnMethods = {"selectFourthContent"})
    public void clickRotateButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Rotate,
                "click rotate button failed");
    }

    @Test(dependsOnMethods = {"clickRotateButton"})
    public void changeImageRotate() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"changeImageRotate"})
    public void clickCancelRotateChanges() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Cancel_Editing,
                "click cancel rotate changes contrast failed");
    }
    @Test(dependsOnMethods = {"clickCancelRotateChanges"})
    public void clickRotateEditButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Rotate,
                "click rotate edit button failed");
    }

    @Test(dependsOnMethods = {"clickRotateEditButton"})
    public void changeImageRotateSettings() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(190,771).moveTo(-84,-4).release().perform();
    }

    @Test(dependsOnMethods = {"changeImageRotateSettings"})
    public void clickRotateEditDoneButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Share_Done_Editing,
                "click rotate edit done button failed");
    }

    @Test(dependsOnMethods = {"clickRotateEditDoneButton"})
    public void clickStickerShareButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Edit_Sticker_Share_Final,
                "click sticker share button");
    }
}
