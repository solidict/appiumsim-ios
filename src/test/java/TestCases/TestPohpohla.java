package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestPohpohla {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void checkToolbarPohPohlaButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_Pohpohla_Button,
                " pohpohla button cannot found");
    }

    @Test(dependsOnMethods = {"checkToolbarPohPohlaButton"})
    public void clickToolbarPohPohlaButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_Pohpohla_Button,
                "click pohpohla button failed");
    }


    @Test(dependsOnMethods = {"clickToolbarPohPohlaButton"})
    public void checkGetOtherButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "getOther button cannot found");
    }

    @Test(dependsOnMethods = {"checkGetOtherButton"})
    public void clickGetOtherButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "click getOther button failed");
    }
    @Test(dependsOnMethods = {"clickGetOtherButton"})
    public void clickGetOtherButton2() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "click getOther button failed");
    }
    @Test(dependsOnMethods = {"clickGetOtherButton2"})
    public void clickGetOtherButton3() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "click getOther button failed");
    }
    @Test(dependsOnMethods = {"clickGetOtherButton3"})
    public void clickGetOtherButton4() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "click getOther button failed");
    }
    @Test(dependsOnMethods = {"clickGetOtherButton4"})
    public void clickGetOtherButton5() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_GetOther_Button,
                "click getOther button failed");
    }

    @Test(dependsOnMethods = {"clickGetOtherButton5"})
    public void checkMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_Menu_Button,
                "menu button cannot found");
    }
    @Test(dependsOnMethods = {"checkMenuButton"})
    public void clickMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Pohpohla_Menu_Button,
                "click menu button failed");
    }

    @Test(dependsOnMethods = {"clickMenuButton"})
    public void clickHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "click home button on leftmenu failed");
    }

}
