package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForKidPartThree {
    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkKidGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkKidGiftBoxButton"})
    public void checkKidForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "check home for kids button failed");
    }

    @Test(dependsOnMethods = {"checkKidForHomeButton"})
    public void clickHomeForKidButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForKids_Button,
                "click home for kid button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForKidButton"})
    public void checkForKidBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check for kid back button failed");
    }

    @Test(dependsOnMethods = {"checkForKidBackButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDown() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(219,572).moveTo(7,-404).release().perform();
        (new TouchAction(driver)).press(205,109).moveTo(-16,458).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDown"})
    public void clickSelectedContent () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(161, 636).perform();
    }

    @Test(dependsOnMethods = {"clickSelectedContent"})
    public void checkHeaderContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check header content back button failed");
    }

    @Test(dependsOnMethods = {"checkHeaderContentBackButton"})
    public void clickFilterButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Filter_Content_Button,
                "Click filter button failed");

    }
    @Test(dependsOnMethods = {"clickFilterButton"})
    public void selectChildByName() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(255, 156).perform();
        (new TouchAction(driver)).press(255,752).moveTo( 11,-60).release().perform();
    }

    @Test(dependsOnMethods = {"selectChildByName"})
    public void selectKidPickerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "Click kid picker done button failed");
        (new TouchAction(driver)).tap(185, 262).perform();
    }

    @Test(dependsOnMethods = {"selectKidPickerDoneButton"})
    public void scrollDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(185,658).moveTo(9,-403).release().perform();
        (new TouchAction(driver)).press(246,163).moveTo(-10,454).release().perform();
    }




    @Test(dependsOnMethods = {"scrollDownAndUp"})
    public void clickFilterContentButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Filter_Again_Button,
                "Click filter content button failed");

    }
    @Test(dependsOnMethods = {"clickFilterContentButton"})
    public void selectChildByAge() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(227, 215).perform();
        (new TouchAction(driver)).press(274,754).moveTo(-1,-72).release().perform();
    }

    @Test(dependsOnMethods = {"selectChildByAge"})
    public void selectKidWithAgePickerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "Click kid with age picker done button failed");
        (new TouchAction(driver)).tap(185, 262).perform();
    }

    @Test(dependsOnMethods = {"selectKidWithAgePickerDoneButton"})
    public void scrollContentDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(185,658).moveTo(9,-403).release().perform();
        (new TouchAction(driver)).press(246,163).moveTo(-10,454).release().perform();
    }
    @Test(dependsOnMethods = {"scrollContentDownAndUp"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click back button failed");
    }
    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "check detail back button failed");
    }

    @Test(dependsOnMethods = {"checkDetailBackButton"})
    public void clickDetailBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Back_Button,
                "click detail back button failed");
    }

}
