package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestForHomePartOne {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }
    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "check home for home button failed");
    }

    @Test(dependsOnMethods = {"checkHomeForHomeButton"})
    public void clickHomeForHomeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_ForHome_Button,
                "click home for home button failed");
    }
    @Test(dependsOnMethods = {"clickHomeForHomeButton"})
    public void checkForHomeGiftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check for home gift button failed");
    }

    @Test(dependsOnMethods = {"checkForHomeGiftButton"})
    public void checkBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"checkBannerRightButton"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }
    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void scrollDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(251,685).moveTo(21,-486).release().perform();
        (new TouchAction(driver)).press(274,194).moveTo(-23,488).release().perform();
    }

    @Test(dependsOnMethods = {"scrollDownAndUp"})
    public void clickRecipes () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(181, 408).perform();
    }

    @Test(dependsOnMethods = {"clickRecipes"})
    public void checkDetailListHeader() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check detail list header failed");
    }

    @Test(dependsOnMethods = {"checkDetailListHeader"})
    public void scrollContentDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(189, 629).moveTo(2, -477).release().perform();
        (new TouchAction(driver)).press(209, 153).moveTo(-18, 483).release().perform();
    }

    @Test(dependsOnMethods = {"scrollContentDownAndUp"})
    public void clickRecipesDetailList () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(178, 191).perform();
    }

    @Test(dependsOnMethods = {"clickRecipesDetailList"})
    public void checkRecipeDetailListHeader() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "Check recipe detail list header failed");
    }

    @Test(dependsOnMethods = {"checkRecipeDetailListHeader"})
    public void scrollRecipeContentDownAndUp() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).press(238,668).moveTo(-15,-471).release().perform();
        (new TouchAction(driver)).press(269,209).moveTo(-23,360).release().perform();
    }

    @Test(dependsOnMethods = {"scrollRecipeContentDownAndUp"})
    public void clickRecipeDetailContent () throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(183, 160).perform();
    }
    @Test(dependsOnMethods = {"clickRecipeDetailContent"})
    public void checkContentDetailHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Header,
                "check content detail header failed");
    }

    @Test(dependsOnMethods = {"checkContentDetailHeader"})
    public void checkPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "check play button failed");
    }

    @Test(dependsOnMethods = {"checkPlayButton"})
    public void clickPlayButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Play_Button,
                "click play button failed");
    }

    @Test(dependsOnMethods = {"clickPlayButton"})
    public void checkPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "check player done button failed");
    }

    @Test(dependsOnMethods = {"checkPlayerDoneButton"})
    public void clickPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "click player done button failed");
    }

    @Test(dependsOnMethods = {"clickPlayerDoneButton"})
    public void checkBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "check back button failed");
    }

    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Content_Detail_Button_Back,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check content back button failed");
    }
    @Test(dependsOnMethods = {"checkContentBackButton"})
    public void clickContentBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click content back button failed");
    }
}
