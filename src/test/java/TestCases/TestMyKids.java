package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestMyKids {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void checkToolbarMyKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_MyKids_Button,
                "check toolbar mykids button cannot found");
    }

    @Test(dependsOnMethods = {"checkToolbarMyKidsButton"})
    public void clickToolbarMyKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_MyKids_Button,
                "click toolbar mykids button cannot found");
    }

    @Test(dependsOnMethods = {"clickToolbarMyKidsButton"})
    public void checkMyKidsGiftButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check my kids gift button failed");
    }
    @Test(dependsOnMethods = {"checkMyKidsGiftButton"})
    public void clickAddChildButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_addChild_Button,
                "click my kids gift button failed");
    }
    @Test(dependsOnMethods = {"clickAddChildButton"})
    public void checkHaveChildButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_haveChild_Button,
                "check have child button failed");
    }

    @Test(dependsOnMethods = {"checkHaveChildButton"})
    public void clickHaveChildButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_haveChild_Button,
                "click have child button failed");
    }

    @Test(dependsOnMethods = {"clickHaveChildButton"})
    public void checkMyKidsGiftTopButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check my kids gift top button failed");
    }

    @Test(dependsOnMethods = {"checkMyKidsGiftTopButton"})
    public void clickMaleButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_male_Button,
                "click male button failed");
    }
    @Test(dependsOnMethods = {"clickMaleButton"})
    public void clickContinueKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue kids button failed");
    }

    @Test(dependsOnMethods = {"clickContinueKidsButton"})
    public void clickDoneKidsInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click done kids info popup button failed");
    }

    @Test(dependsOnMethods = {"clickDoneKidsInfoPopUpButton"})
    public void typeKidNameSurname() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Kids_NameSurname_Textfield,
                iOSConstants.NEW_KID_NAME,
                "type kid name surname not found" );
    }

    @Test(dependsOnMethods = {"typeKidNameSurname"})
    public void clickContinueToBirthdayButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue kids button failed");
    }
    @Test(dependsOnMethods = {"clickContinueToBirthdayButton"})
    public void clickDoneForKidsInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click done for kids info popup button failed");
    }

    @Test(dependsOnMethods = {"clickDoneForKidsInfoPopUpButton"})
    public void enterDayForBirthdayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(65, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterDayForBirthdayForKid"})
    public void clickDoneForKidsDayPickerInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done for kids day picker info popup failed");
    }


    @Test(dependsOnMethods = {"clickDoneForKidsDayPickerInfoPopUpButton"})
    public void enterMonthForBirthdayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(179, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterMonthForBirthdayForKid"})
    public void clickDoneForKidsMonthPickerInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done for kids month picker info popup failed");
    }


    @Test(dependsOnMethods = {"clickDoneForKidsMonthPickerInfoPopUpButton"})
    public void enterYearForBirthdayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(308, 524).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterYearForBirthdayForKid"})
    public void clickDoneForKidsYearPickerInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done for kids year picker info popup button failed");
    }
    @Test(dependsOnMethods = {"clickDoneForKidsYearPickerInfoPopUpButton"})
    public void clickContinueButtonNext() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue button next failed");
    }

    @Test(dependsOnMethods = {"clickContinueButtonNext"})
    public void clickFirstChild() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(134, 121).perform();
    }

    @Test(dependsOnMethods = {"clickFirstChild"})
    public void checkSeeKidDevelopmentButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "check see kid development button failed");
    }

    @Test(dependsOnMethods = {"checkSeeKidDevelopmentButton"})
    public void clickSeeKidDevelopmentButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "click see kid development button failed");
    }

    @Test(dependsOnMethods = {"clickSeeKidDevelopmentButton"})
    public void checkGiftBoxButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check gift box button failed");
    }
    @Test(dependsOnMethods = {"checkGiftBoxButton"})
    public void checkBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check back button failed");
    }
    @Test(dependsOnMethods = {"checkBackButton"})
    public void clickBackButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click back button failed");
    }

    @Test(dependsOnMethods = {"clickBackButton"})
    public void checkBackToKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "check back kids button failed");
    }
    @Test(dependsOnMethods = {"checkBackToKidsButton"})
    public void clickBackKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click back kids button failed");
    }
    @Test(dependsOnMethods = {"clickBackKidsButton"})
    public void checkKidsListGiftBoxButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check kids list gift box button failed");
    }

    @Test(dependsOnMethods = {"checkKidsListGiftBoxButton"})
    public void clickSeeMoreButtonsAndTapEdit() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(350, 121).perform();
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Edit_ChildInfo,
                "click edit child info button failed");
    }
    @Test(dependsOnMethods = {"clickSeeMoreButtonsAndTapEdit"})
    public void checkChildProfileGiftBoxButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check child profile gift box button failed");
    }

    @Test(dependsOnMethods = {"checkChildProfileGiftBoxButton"})
    public void typeKidNameSurnameUpdate() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Kids_NameSurname_Textfield,
                iOSConstants.NEW_KID_NAME,
                "type kid name surname update not found" );
    }

    @Test(dependsOnMethods = {"typeKidNameSurnameUpdate"})
    public void clickContinueToUpdateButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue to update button failed");
    }

    @Test(dependsOnMethods = {"clickContinueToUpdateButton"})
    public void clickOKInfoButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "click ok info button failed");
    }
    @Test(dependsOnMethods = {"clickOKInfoButton"})
    public void clickUpdateSeeMoreButtonsAndTapEdit() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(350, 121).perform();
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Edit_ChildInfo,
                "click update see more button failed");
    }

    @Test(dependsOnMethods = {"clickUpdateSeeMoreButtonsAndTapEdit"})
    public void checkUpdateChildProfileGiftBoxButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check update child profile gift box button failed");
    }

    @Test(dependsOnMethods = {"checkUpdateChildProfileGiftBoxButton"})
    public void enterUpdateDayForBirthdayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(65, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();
    }

    @Test(dependsOnMethods = {"enterUpdateDayForBirthdayForKid"})
    public void clickUpdateDoneForKidsDayPickerInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done for kids day picker info popup failed");
    }

    @Test(dependsOnMethods = {"clickUpdateDoneForKidsDayPickerInfoPopUpButton"})
    public void clickUpdateContinueToUpdateButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click update continue to update button failed");
    }

    @Test(dependsOnMethods = {"clickUpdateContinueToUpdateButton"})
    public void clickUpdateOKInfoButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "click update ok info button failed");
    }


    @Test(dependsOnMethods = {"clickUpdateOKInfoButton"})
    public void clickUpdateGenderSeeMoreButtonsAndTapEdit() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(350, 121).perform();
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Edit_ChildInfo,
                "click update gender see more button failed");
    }

    @Test(dependsOnMethods = {"clickUpdateGenderSeeMoreButtonsAndTapEdit"})
    public void checkUpdateGenderChildProfileGiftBoxButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check update gender child profile gift box button failed");
    }

    @Test(dependsOnMethods = {"checkUpdateGenderChildProfileGiftBoxButton"})
    public void clickUpdatFemaleButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_female_Button,
                "click female button failed");
    }
    @Test(dependsOnMethods = {"clickUpdatFemaleButton"})
    public void clickUpdateContinueKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click update continue kids button failed");
    }

    @Test(dependsOnMethods = {"clickUpdateContinueKidsButton"})
    public void clickOptionsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(349, 124).perform();
    }
    @Test(dependsOnMethods = {"clickOptionsButton"})
    public void checkRemoveKidButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Remove_Child,
                "check remove kid button failed");
    }
    @Test(dependsOnMethods = {"checkRemoveKidButton"})
    public void clickRemoveKidButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Remove_Child,
                "click remove kid button failed");
    }

    @Test(dependsOnMethods = {"clickRemoveKidButton"})
    public void clickOKKidInfoPopupButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click ok kid info popup button failed");
    }

    @Test(dependsOnMethods = {"clickOKKidInfoPopupButton"})
    public void clickAddChildButtonNew() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_addChild_Button,
                "click add child button failed");
    }
    @Test(dependsOnMethods = {"clickAddChildButtonNew"})
    public void checkHaveChildButtonOnPage() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_haveChild_Button,
                "check have child button on page failed");
    }

    @Test(dependsOnMethods = {"checkHaveChildButtonOnPage"})
    public void clickHaveAlreadyChildButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_haveChild_Button,
                "click have already child button failed");
    }

    @Test(dependsOnMethods = {"clickHaveAlreadyChildButton"})
    public void checkMyKidsGiftTopBarButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check my kids gift top bar button failed");
    }

    @Test(dependsOnMethods = {"checkMyKidsGiftTopBarButton"})
    public void clickFemaleButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_female_Button,
                "click female button failed");
    }
    @Test(dependsOnMethods = {"clickFemaleButton"})
    public void clickContinueKToidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue to kids button failed");
    }

    @Test(dependsOnMethods = {"clickContinueKToidsButton"})
    public void clickDoneKidsPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click done kids info popup button failed");
    }

    @Test(dependsOnMethods = {"clickDoneKidsPopUpButton"})
    public void typeKidNameAndSurname() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Kids_NameSurname_Textfield,
                iOSConstants.NEW_KID_NAME,
                "type kid name and surname not found" );
    }

    @Test(dependsOnMethods = {"typeKidNameAndSurname"})
    public void clickContinueBirthdayButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue birthday button failed");
    }
    @Test(dependsOnMethods = {"clickContinueBirthdayButton"})
    public void clickDoneInfoPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click done info popup button failed");
    }

    @Test(dependsOnMethods = {"clickDoneInfoPopUpButton"})
    public void enterDayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(65, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterDayForKid"})
    public void clickDonePickerPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done picker info popup failed");
    }


    @Test(dependsOnMethods = {"clickDonePickerPopUpButton"})
    public void enterMonthForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(179, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterMonthForKid"})
    public void clickDonePopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done popup button failed");
    }


    @Test(dependsOnMethods = {"clickDonePopUpButton"})
    public void enterYearForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(308, 524).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterYearForKid"})
    public void clickPickerDonePopupButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click picker popup button failed");
    }
    @Test(dependsOnMethods = {"clickPickerDonePopupButton"})
    public void clickButtonNext() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click button next failed");
    }

    @Test(dependsOnMethods = {"clickButtonNext"})
    public void clickKidAdded() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(137, 122).perform();
    }

    @Test(dependsOnMethods = {"clickKidAdded"})
    public void checkSeeChildCard() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "check see child card button failed");
    }
    @Test(dependsOnMethods = {"checkSeeChildCard"})
    public void clickSeeChildCard() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "click see child card button failed");
    }
    @Test(dependsOnMethods = {"clickSeeChildCard"})
    public void clickBackPrevious() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click back previous button failed");
    }

    @Test(dependsOnMethods = {"clickBackPrevious"})
    public void checkAddDataChild() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Add_Data_Child,
                "check add data child button failed");
    }

    @Test(dependsOnMethods = {"checkAddDataChild"})
    public void clickAddDataChild() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Add_Data_Child,
                "click add data child button failed");
    }
    @Test(dependsOnMethods = {"clickAddDataChild"})
    public void checkUpdateChildInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Update_ChildInfo_Button,
                "check child info button failed");
    }

    @Test(dependsOnMethods = {"checkUpdateChildInfo"})
    public void typeHeight() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Child_Height,
                iOSConstants.CHILD_HEIGHT,
                "textfield height not found" );
    }

    @Test(dependsOnMethods = {"typeHeight"})
    public void typeWeight() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Child_Weight,
                iOSConstants.CHILD_WEIGHT,
                "textfield weight not found" );
    }

    @Test(dependsOnMethods = {"typeWeight"})
    public void clickUpdateChildInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Update_ChildInfo_Button,
                "click child info button failed");
    }

    @Test(dependsOnMethods = {"clickUpdateChildInfo"})
    public void clickConfirmInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Info_Popup_OK_Button,
                "click confirm info button failed");
    }
    @Test(dependsOnMethods = {"clickConfirmInfo"})
    public void clickBackToButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click back to button failed");
    }










    @Test(dependsOnMethods = {"clickBackToButton"})
    public void clickAddPregnantChildButtonNew() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_addChild_Button,
                "click add pregnant child button failed");
    }
    @Test(dependsOnMethods = {"clickAddPregnantChildButtonNew"})
    public void checkHaveChildPage() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_haveChild_Button,
                "check have child  page failed");
    }

    @Test(dependsOnMethods = {"checkHaveChildPage"})
    public void clickPregnantButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pregnant_Button,
                "click pregnant button failed");
    }

    @Test(dependsOnMethods = {"clickPregnantButton"})
    public void checkKidsGiftTopBarButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check kids gift top bar button failed");
    }

    @Test(dependsOnMethods = {"checkKidsGiftTopBarButton"})
    public void clickNoGenderButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_No_Gender,
                "click no gender button failed");
    }
    @Test(dependsOnMethods = {"clickNoGenderButton"})
    public void clickContinueKidsPage() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue kids page failed");
    }

    @Test(dependsOnMethods = {"clickContinueKidsPage"})
    public void clickDoneToKidsButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click done to kids button failed");
    }

    @Test(dependsOnMethods = {"clickDoneToKidsButton"})
    public void typeMyKidNameAndSurname() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Kids_NameSurname_Textfield,
                iOSConstants.NEW_KID_NAME,
                "type my kid name and surname not found" );
    }

    @Test(dependsOnMethods = {"typeMyKidNameAndSurname"})
    public void clickContinueButtonBirthday() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click continue button birthday failed");
    }
    @Test(dependsOnMethods = {"clickContinueButtonBirthday"})
    public void clickPopUpInfoButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_Popup_OK_Button,
                "click info popup button failed");
    }

    @Test(dependsOnMethods = {"clickPopUpInfoButton"})
    public void enterBabyDayForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(65, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterBabyDayForKid"})
    public void clickBabyDonePickerPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click baby done picker info popup failed");
    }


    @Test(dependsOnMethods = {"clickBabyDonePickerPopUpButton"})
    public void enterBabyMonthForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(179, 523).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterBabyMonthForKid"})
    public void clickDoneBabyPopUpButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click done baby popup button failed");
    }


    @Test(dependsOnMethods = {"clickDoneBabyPopUpButton"})
    public void enterBabyYearForKid() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(308, 524).perform();
        (new TouchAction(driver)).press(300,752).moveTo(2,-111).release().perform();

    }

    @Test(dependsOnMethods = {"enterBabyYearForKid"})
    public void clickPickerBabyDonePopupButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_pickerDoneButton,
                "click picker baby done popup button failed");
    }
    @Test(dependsOnMethods = {"clickPickerBabyDonePopupButton"})
    public void clickBabyButtonNext() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_continue_Kids_Button,
                "click babt button next failed");
    }

    @Test(dependsOnMethods = {"clickBabyButtonNext"})
    public void clickBabyKidAdded() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(137, 122).perform();
    }

    @Test(dependsOnMethods = {"clickBabyKidAdded"})
    public void checkBabySeeChildCard() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "check baby see child card button failed");
    }
    @Test(dependsOnMethods = {"checkBabySeeChildCard"})
    public void clickBabySeeChildCard() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Kids_See_Development,
                "click baby see child card button failed");
    }
    @Test(dependsOnMethods = {"clickBabySeeChildCard"})
    public void clickBabyBackPrevious() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click baby back previous button failed");
    }

    @Test(dependsOnMethods = {"clickBabyBackPrevious"})
    public void checkBabyAddDataChild() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Add_Data_Child,
                "check baby add data child button failed");
    }

    @Test(dependsOnMethods = {"checkBabyAddDataChild"})
    public void clickBabyAddDataChild() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Kids_Add_Data_Child,
                "click baby add data child button failed");
    }
    @Test(dependsOnMethods = {"clickBabyAddDataChild"})
    public void checkBabyUpdateChildInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Update_ChildInfo_Button,
                "check baby child info button failed");
    }

    @Test(dependsOnMethods = {"checkBabyUpdateChildInfo"})
    public void typeBabyHeight() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Child_Height,
                iOSConstants.CHILD_HEIGHT,
                "textfield height not found" );
    }

    @Test(dependsOnMethods = {"typeBabyHeight"})
    public void typeBabyWeight() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Child_Weight,
                iOSConstants.CHILD_WEIGHT,
                "textfield weight not found" );
    }

    @Test(dependsOnMethods = {"typeBabyWeight"})
    public void clickBabyUpdateChildInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Update_ChildInfo_Button,
                "click baby update child ingo failed");
    }

    @Test(dependsOnMethods = {"clickBabyUpdateChildInfo"})
    public void clickBabyConfirmInfo() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Info_Popup_OK_Button,
                "click baby confirm info button failed");
    }
    @Test(dependsOnMethods = {"clickBabyConfirmInfo"})
    public void clickBabyBackToButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Verification_Back_Button,
                "click baby back to button failed");
    }
    @Test(dependsOnMethods = {"clickBabyConfirmInfo"})
    public void checkLeftMenu() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Button_ContactUs,
                "check leftmenu failed");
    }
    @Test(dependsOnMethods = {"clickBabyConfirmInfo"})
    public void clickLeftMenu() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Button_ContactUs,
                "click leftmenu failed");
    }
    @Test(dependsOnMethods = {"clickLeftMenu                                                                                                                                                                                                 "})
    public void clickHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "click home on left menu failed");

    }
}
