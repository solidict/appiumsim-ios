package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestLoginFail {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Test(dependsOnMethods = {"setup"})
    public void clickLoginSplashButton() throws MalformedURLException {
        //Click Giris Yap Button
        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_LoginSplash_Login_Button,
                "Splash login button not found");

    }

    @Test(dependsOnMethods = {"clickLoginSplashButton"})
    public void typeGSMNumber() throws MalformedURLException {

        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textField_GSMNumber,
                iOSConstants.FALSE_MSISDN,
                "textfield GSM not found" );
    }

    @Test(dependsOnMethods = {"typeGSMNumber"})
    public void clickLoginButton() throws MalformedURLException {

        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_Login_Bg_Splash_JPG,
                "click empty area failed!");

        iosDriver.IosDevice().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_Login_LoginButton,
                "Login click button on login page failed");

    }

    @Test(dependsOnMethods = {"clickLoginButton"})
    public void deleteGSMNumber() throws MalformedURLException {

        appiumUtils.typeData(driver,
               "",
                iOSConstants.FALSE_MSISDN,
                "textfield GSM not found" );
    }

    @Test(dependsOnMethods = {"deleteGSMNumber"})
    public void typePassword() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_textField_GSMPassword,
                iOSConstants.FALSE_PASSWORD,
                "textfield password not found" );
    }

    @Test(dependsOnMethods = {"typePassword"})
    public void clickLoginButton2() throws MalformedURLException {

        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_Login_Bg_Splash_JPG,
                "click empty area failed!");

        iosDriver.IosDevice().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_Login_LoginButton,
                "Login click button on login page failed");

    }
}
