package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestContactUs {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "click home menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void checkLeftMenuHelpButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Help,
                "Help button on left menu cannot found");
    }
    @Test(dependsOnMethods = {"checkLeftMenuHelpButton"})
    public void clickLeftMenuHelpButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Help,
                "Click Help button on left menu failed");
    }

    @Test(dependsOnMethods = {"clickLeftMenuHelpButton"})
    public void clickContactUsButton() throws MalformedURLException {

        (new TouchAction(driver)).tap(124, 730).perform();
    }

    @Test(dependsOnMethods = {"clickContactUsButton"})
    public void checkSendButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Send_Button,
                "Send button cannot found");
    }
    @Test(dependsOnMethods = {"checkSendButton"})
    public void clickSendButton() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Send_Button,
                "Click Send button failed");
    }

    @Test(dependsOnMethods = {"clickSendButton"})
    public void checkOKButtonOnPopUp() throws MalformedURLException {

        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Popup_OK_Button,
                "OK button on Popup cannot found");
    }

    @Test(dependsOnMethods = {"checkOKButtonOnPopUp"})
    public void clickOKButtonOnPopUp() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Popup_OK_Button,
                "Click OK button failed");
    }

    @Test(dependsOnMethods = {"clickOKButtonOnPopUp"})
    public void typeQuestion() throws MalformedURLException {

        appiumUtils.typeData(
                driver,
                SimIOSData.accessibilityID_Textview,
                iOSConstants.EXAMPLE_TEXT,
                "textview not found" );
    }
    @Test(dependsOnMethods = {"typeQuestion"})
    public void clickKeyboardDoneButton() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "Click keyboard done button failed");
    }
    @Test(dependsOnMethods = {"clickKeyboardDoneButton"})
    public void clickSendButton2() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Send_Button,
                "Click Send button failed");
    }

    @Test(dependsOnMethods = {"clickSendButton2"})
    public void clickOKButtonOnPopUp2() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Popup_OK_Button,
                "Click OK button on pop up failed");
    }
    @Test(dependsOnMethods = {"clickOKButtonOnPopUp2"})
    public void clickLeftMenuButtonOnContactUs() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Button_ContactUs,
                "Click left menu on contact us failed");
    }
    @Test(dependsOnMethods = {"clickLeftMenuButtonOnContactUs"})
    public void clickHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "Click home on left menu failed");
    }

}
