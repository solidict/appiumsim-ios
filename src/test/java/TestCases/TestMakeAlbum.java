package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestMakeAlbum {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void checkToolbarMakeAlbumButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_MakeAlbum_Button,
                "check Toolbar edit share button failed");
    }

    @Test(dependsOnMethods = {"checkToolbarMakeAlbumButton"})
    public void clickToolbarMakeAlbumButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessiblityID_Toolbar_MakeAlbum_Button,
                "click Toolbar edit share button failed");
    }

    @Test(dependsOnMethods = {"clickToolbarMakeAlbumButton"})
    public void checkToolbarMakeAlbumHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Header,
                "check make album header failed");
    }

    @Test(dependsOnMethods = {"checkToolbarMakeAlbumHeader"})
    public void clickAddAlbumButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(101, 204).perform();
    }

    @Test(dependsOnMethods = {"clickAddAlbumButton"})
    public void clickSelectFromCameraRollButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Select_From_CameraRoll,
                "click select from camera roll button failed");
    }

    @Test(dependsOnMethods = {"clickSelectFromCameraRollButton"})
    public void selectImageFromCameraRoll() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Camera_Roll,
                "select image from camera roll failed");
    }

    @Test(dependsOnMethods = {"selectImageFromCameraRoll"})
    public void selectFirstImage() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(42, 132).perform();
    }

    @Test(dependsOnMethods = {"selectFirstImage"})
    public void selectThirdFilter() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(171, 620).perform();
    }

    @Test(dependsOnMethods = {"selectThirdFilter"})
    public void clickAddTextForAlbum() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Add_Text,
                "click add text for album failed");
    }
    @Test(dependsOnMethods = {"clickAddTextForAlbum"})
    public void typeAlbumText() throws MalformedURLException {
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Make_Album_Textfield,
                iOSConstants.TEXT_EDIT_TEXT,
                "textfield typeAlbumText not found" );
    }
    @Test(dependsOnMethods = {"typeAlbumText"})
    public void clickTextfieldDoneButtonMakeAlbum() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Textfield_Done,
                "click textfield done button make album failed");
    }

    @Test(dependsOnMethods = {"clickTextfieldDoneButtonMakeAlbum"})
    public void clickTurnImageMakeAlbum() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Turn_Image,
                "click turn image  make album failed");
    }

    @Test(dependsOnMethods = {"clickTurnImageMakeAlbum"})
    public void clickContinueButtonMakeAlbum() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Continue_Button,
                "click continue button make album failed");
    }



    @Test(dependsOnMethods = {"clickContinueButtonMakeAlbum"})
    public void clickAddAlbumImageButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(276, 346).perform();
    }

    @Test(dependsOnMethods = {"clickAddAlbumImageButton"})
    public void clickSelectFromCameraRollImageButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Select_From_CameraRoll,
                "click select from camera roll image button failed");
    }

    @Test(dependsOnMethods = {"clickSelectFromCameraRollImageButton"})
    public void selectNewImageFromCameraRoll() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Camera_Roll,
                "select new image from camera roll failed");
    }

    @Test(dependsOnMethods = {"selectNewImageFromCameraRoll"})
    public void selectSecondCameraRollImage() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(134, 137).perform();
    }

    @Test(dependsOnMethods = {"selectSecondCameraRollImage"})
    public void selectFirstFilter() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        (new TouchAction(driver)).tap(46, 616).perform();
    }

    @Test(dependsOnMethods = {"selectFirstFilter"})
    public void clickFinishAlbum() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Continue_Button,
                "click finish failed");
    }

    @Test(dependsOnMethods = {"clickFinishAlbum"})
    public void checkPlayMakeAlbumHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Header,
                "check play make album failed");
    }

    @Test(dependsOnMethods = {"checkPlayMakeAlbumHeader"})
    public void clickPlayMakeAlbum() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Make_Album_Button_Play,
                "click play make album failed");
    }

    @Test(dependsOnMethods = {"clickPlayMakeAlbum"})
    public void clickPlayerDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Player_Done_Button,
                "click player done button failed");
    }


}
