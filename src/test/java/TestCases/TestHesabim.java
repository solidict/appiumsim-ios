package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestHesabim {


    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "check home menu button");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "click home left menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void clickHesabimButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Hesabim,
                "Click hesabim button failed");

    }

    @Test(dependsOnMethods = {"clickHesabimButton"})
    public void checkHeaderOnHesabim() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Header_Hesabim,
                "check header on hesabim failed");
    }

    @Test(dependsOnMethods = {"checkHeaderOnHesabim"})
    public void scrollInvoicesAndDetails() throws MalformedURLException {

        (new TouchAction(driver))
                .press(341, 187).moveTo(-278, 1).release().perform();

        (new TouchAction(driver))
                .press(343, 551).moveTo(2,-355).release().perform();

        (new TouchAction(driver))
                .press(335, 343).moveTo(-261,0).release().perform();
    }

    @Test(dependsOnMethods = {"scrollInvoicesAndDetails"})
    public void checkInvoiceDetailButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_InvoiceDetail_button,
                "check invoice detail button failed");

    }
    @Test(dependsOnMethods = {"checkInvoiceDetailButton"})
    public void clickInvoiceDetailButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_InvoiceDetail_button,
                "check invoice detail back button failed");

    }

    @Test(dependsOnMethods = {"clickInvoiceDetailButton"})
    public void checkInvoiceDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_InvoiceDetail_back_button,
                "check invoice detail back button failed");

    }
    @Test(dependsOnMethods = {"checkInvoiceDetailBackButton"})
    public void clickInvoiceDetailBackButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_InvoiceDetail_back_button,
                "click invoice detail back button failed");

    }

    @Test(dependsOnMethods = {"clickInvoiceDetailBackButton"})
    public void clickLeftMenuHesabim() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Hesabim,
                "click left menu  hesabim failed");

    }

    @Test(dependsOnMethods = {"clickLeftMenuHesabim"})
    public void checkHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "check home on left menu failed");

    }
    @Test(dependsOnMethods = {"checkHomeOnLeftMenu"})
    public void clickHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Home,
                "click home on left menu failed");

    }
}
