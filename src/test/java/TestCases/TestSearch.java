package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestSearch {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "home menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeLeftMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "click home menu button failed");
    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void tapSearchBar() throws MalformedURLException {
        (new TouchAction(driver)).tap(56, 177).perform();
    }

    @Test(dependsOnMethods = {"tapSearchBar"})
    public void checkSearchHeader() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver,
                SimIOSData.accessibilityID_Search_Header,
                "header on search cannot found");
    }

    @Test(dependsOnMethods = {"checkSearchHeader"})
    public void tapSearchTextfield() throws MalformedURLException {
        (new TouchAction(driver)).tap(47, 102).perform();
        MobileElement el2 = (MobileElement) driver.findElementByXPath(
                "//XCUIElementTypeApplication[@name=\"SİM\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField");
        el2.sendKeys(iOSConstants.SEARCH_EXAMPLE);
    }


}
