package TestCases;

import Utils.AppiumUtils;
import Utils.SimIOSData;
import Utils.TestDriverIOS;
import Utils.iOSConstants;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestAyricaliklarimPartTwo {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test(dependsOnMethods = {"setup"})
    public void checkHomeGiftBoxButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_Home_GiftBox,
                "check home gift box button failed");
    }

    @Test(dependsOnMethods = {"checkHomeGiftBoxButton"})
    public void checkHomeMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.checkElement(driver, SimIOSData.accessibilityID_Home_MenuIcon,
                "Home menu left menu button cannot found");
    }

    @Test(dependsOnMethods = {"checkHomeMenuButton"})
    public void clickHomeLeftMenuButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Home_MenuIcon,
                "click home left menu button failed");

    }

    @Test(dependsOnMethods = {"clickHomeLeftMenuButton"})
    public void clickOpportunitiesButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_LeftMenu_Opportunities,
                "Click opportunities button failed");

    }

    @Test(dependsOnMethods = {"clickOpportunitiesButton"})
    public void checkLeftMenuButtonOnOpportunityVC() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Menu_Button,
                "check left menu button on opportunity vc failed");
    }

    @Test(dependsOnMethods = {"checkLeftMenuButtonOnOpportunityVC"})
    public void clickBannerRightButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Right,
                "Click banner right button failed");

    }

    @Test(dependsOnMethods = {"clickBannerRightButton"})
    public void clickBannerLeftButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityVC_Banner_Swipe_Left,
                "Click banner left button failed");

    }

    @Test(dependsOnMethods = {"clickBannerLeftButton"})
    public void clickFirstElementOnList() throws MalformedURLException {

        (new TouchAction(driver)).tap(147, 386).perform();
    }

    @Test(dependsOnMethods = {"clickFirstElementOnList"})
    public void checkGetCodeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Get_Password_Button,
                "Check get code button failed");

    }

    @Test(dependsOnMethods = {"checkGetCodeButton"})
    public void clickGetCodeButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_OpportunityDetailVC_Get_Password_Button,
                "click get code button failed");
    }

    @Test(dependsOnMethods = {"clickGetCodeButton"})
    public void checkVerifyButtonBeforeTC() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.checkElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "check verify button before tc failed");
    }

    @Test(dependsOnMethods = {"checkVerifyButtonBeforeTC"})
    public void clickVerifyButtonBeforeTC() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "Click verify button before tc failed");
    }

    @Test(dependsOnMethods = {"clickVerifyButtonBeforeTC"})
    public void clickOKButtonOnInfoPopup() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Warning_Popup_OK_Button,
                "Click ok button on info pop up  failed");
    }

    @Test(dependsOnMethods = {"clickOKButtonOnInfoPopup"})
    public void typeTC() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Verify_TCKN_textfield,
                iOSConstants.VERIFY_TC,
                "type tc failed");
    }

    @Test(dependsOnMethods = {"typeTC"})
    public void clickKeyboardDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "click keyboard done button");
    }

    @Test(dependsOnMethods = {"clickKeyboardDoneButton"})
    public void clickVerifyButtonBeforeBirthday() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "Click verify button before birthday failed");
    }

    @Test(dependsOnMethods = {"clickVerifyButtonBeforeBirthday"})
    public void clickOKButtonOnInfoPopupBeforeBirthday() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Warning_Popup_OK_Button,
                "Click ok button on info pop up birthday failed");
    }

    @Test(dependsOnMethods = {"clickOKButtonOnInfoPopupBeforeBirthday"})
    public void clickBirthdayTextfield() throws MalformedURLException {

        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verify_Birthday_textfield,
                "click birthday textfield failed");
    }
    @Test(dependsOnMethods = {"clickBirthdayTextfield"})
    public void clickBirthdayKeyboardDoneButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Keyboard_Done_Button,
                "click birthday keyboard done button");
    }

    @Test(dependsOnMethods = {"clickBirthdayKeyboardDoneButton"})
    public void clickVerifyButtonBeforeName() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "Click verify button before name failed");
    }

    @Test(dependsOnMethods = {"clickVerifyButtonBeforeName"})
    public void clickOKButtonOnInfoPopupBeforeName() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Warning_Popup_OK_Button,
                "Click ok button on info pop up name failed");
    }
    @Test(dependsOnMethods = {"clickOKButtonOnInfoPopupBeforeName"})
    public void typeName() throws MalformedURLException {

        appiumUtils.typeData(driver,
                SimIOSData.accessibilityID_Verify_Name_textfield,
                iOSConstants.VERIFY_NAME,
                "type name failed");
    }

    @Test(dependsOnMethods = {"typeName"})
    public void clickVerifyButtonBeforeSurname() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "Click verify button before surname failed");
    }

    @Test(dependsOnMethods = {"clickVerifyButtonBeforeSurname"})
    public void clickOKButtonOnInfoPopupBeforeSurname() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_Verification_Warning_Popup_OK_Button,
                "Click ok button on info pop up surname failed");
    }
    @Test(dependsOnMethods = {"clickOKButtonOnInfoPopupBeforeSurname"})
    public void typeSurname() throws MalformedURLException {

        appiumUtils.typeData(
                driver,
                SimIOSData.accessibilityID_Verify_Surname_textfield,
                iOSConstants.VERIFY_SURNAME,
                "type surname failed");
    }

    @Test(dependsOnMethods = {"typeSurname"})
    public void clickVerifyButtonBeforeGenderSelection() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        appiumUtils.clickElement(
                driver,
                SimIOSData.accessibilityID_verify_Button,
                "Click verify button before gender selection failed");
    }
}
