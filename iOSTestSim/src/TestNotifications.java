import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestNotifications {


    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Test(dependsOnMethods = {"setup"})
    public void clickNotificationsButton() throws MalformedURLException {

        iosDriver.IosDevice().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(iosDriver.IosDevice(), SimIOSData.accessibilityID_Login_LoginButton, "ayricaliklarim button cannot found");

    }

    @Test(dependsOnMethods = {"clickNotificationsButton"})
    public void clickMenuOnNotifications() throws MalformedURLException {

        iosDriver.IosDevice().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(iosDriver.IosDevice(), SimIOSData.accessibilityID_LeftMenu_Home, "back button cannot found");
    }

}
