import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestAyricaliklarim {


    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }


    @Test(dependsOnMethods = {"setup"})
    public void clickAyricaliklarimButton() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LeftMenu_Opportunities, "ayricaliklarim button cannot found");

    }

    @Test(dependsOnMethods = {"clickAyricaliklarimButton"})
    public void clickMenuOnAyricaliklarim() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_OpportunityVC_Menu_Button, "opportunities menu button cannot found");
    }

    @Test(dependsOnMethods = {"clickMenuOnAyricaliklarim"})
    public void clickHomeOnLeftMenu() throws MalformedURLException {

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_OpportunityVC_Menu_Button, "left menu home button cannot found");
    }

}
