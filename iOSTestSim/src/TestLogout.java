import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestLogout {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

    public TestLogout() {
    }

    @Test
    public void setup(){
        try {
            driver = iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Test(
            dependsOnMethods = {"setup"}
    )
    public void clickMenuIcon() throws MalformedURLException {
        driver.manage().timeouts().pageLoadTimeout(60L, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Home_MenuIcon, "Menu button cannot found");
    }

    @Test(
            dependsOnMethods = {"clickMenuIcon"}
    )
    public void clickCikisButton() throws MalformedURLException {
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LeftMenu_CikisButton, "Logout button cannot found");
    }

    @Test(
            dependsOnMethods = {"clickCikisButton"}
    )
    public void clickPopUpCikisYapButton() throws MalformedURLException {
        driver.manage().timeouts().implicitlyWait(30L, TimeUnit.SECONDS);
        appiumUtils.clickElement(driver, SimIOSData.accessibilityID_PopUpCikisYapButton, "Popup logout button cannot found");
    }

}
