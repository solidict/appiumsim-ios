public class SimIOSData {


    // Accessibility ID's

    // Login Splash
    public static String accessibilityID_LoginSplash_Login_Button="loginSplashLogin";

    // Login
    public static String accessibilityID_textField_GSMNumber= "phoneTextField";
    public static String accessibilityID_textField_GSMPassword= "passwordTextField";
    public static String accessibilityID_Login_LoginButton="tapToLogin";
    public static String accessibilityID_Login_Bg_Splash_JPG="bg_splash.jpg";

    // Home
    public static String accessibilityID_Home_MenuIcon= "homeLeftMenu";

    // Left Menu
    public static String accessibilityID_LeftMenu_Home = "Ana Sayfa";
    public static String accessibilityID_LeftMenu_Opportunities = "Ayrıcalıklarım";
    public static String accessibilityID_LeftMenu_CikisButton = "Çıkış";
    public static String accessibilityID_PopUpCikisYapButton = "popupLogoutButton";

    // Ayricaliklarim
    public static String accessibilityID_OpportunityVC_Back_Button= "opportunityBackButton";
    public static String accessibilityID_OpportunityVC_Menu_Button= "opportunityMenuButton";

    // Notifications
    public static String accessibilityID_Notifications_Menu_Button= "menuNotifications";



}
