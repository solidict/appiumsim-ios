
import io.appium.java_client.ios.IOSDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.bouncycastle.cms.RecipientId.password;

public class TestLogin  {

    AppiumUtils appiumUtils = new AppiumUtils();
    TestDriverIOS iosDriver = new TestDriverIOS();
    IOSDriver driver;

        @Test
        public void setup(){
            try {
                driver = iosDriver.IosDevice();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

        }

        @Test(dependsOnMethods = {"setup"})
        public void checkSplashLoginButton() throws MalformedURLException {
            driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
            appiumUtils.clickElement(driver, SimIOSData.accessibilityID_LoginSplash_Login_Button, "Menu button cannot found");
    }

        @Test(dependsOnMethods = {"checkSplashLoginButton"})
        public void clickSplashLoginButton() throws MalformedURLException {
            //Click Giris Yap Button
            appiumUtils.checkElement(driver, SimIOSData.accessibilityID_LoginSplash_Login_Button, "Splash login button not found");

        }

        @Test(dependsOnMethods = {"clickSplashLoginButton"})
        public void typeGSMNumber() throws MalformedURLException {

            appiumUtils.typeData(driver,
                    SimIOSData.accessibilityID_textField_GSMNumber, iOSConstants.MSISDN, "textfield GSM not found" );
        }

        @Test(dependsOnMethods = {"typeGSMNumber"})
        public void typePassword() throws MalformedURLException {
            appiumUtils.typeData(driver,
                    SimIOSData.accessibilityID_textField_GSMPassword, iOSConstants.PASSWORD, "textfield password not found" );
        }

        @Test(dependsOnMethods = {"typePassword"})
        public void clickLoginButton() throws MalformedURLException {

            appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Login_Bg_Splash_JPG, "click empty area failed!");

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            appiumUtils.clickElement(driver, SimIOSData.accessibilityID_Login_LoginButton, "Login click button on login page failed");
        }

}
