
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import org.testng.Assert;

import java.net.MalformedURLException;

public class AppiumUtils {

    public Boolean checkElement(IOSDriver driver, String accessibilityId, String message){

        try {

            MobileElement element= (MobileElement) driver.findElementByAccessibilityId(accessibilityId);
            if(element.isEnabled() || element.isDisplayed()){
                return  true;

            }

        }catch (Exception e){
            Assert.fail(message);
        }
        Assert.fail(message);
        return false;
    }


    public void clickElement(IOSDriver driver, String accessibilityId, String message) throws MalformedURLException {

        if (checkElement(driver,accessibilityId,message)) {
            driver.findElementByAccessibilityId(accessibilityId).click();
        }
    }

    public void typeData(IOSDriver driver, String accessibilityId, String data, String message) {

        try {
            if (checkElement(driver, accessibilityId, message)) {

                driver.findElementByAccessibilityId(accessibilityId).sendKeys(data);
            }
        } catch (Exception e) {

            Assert.fail(message);
        }
    }
}
