import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class AccessibilityIdMethods {

 public Boolean CheckElement(IOSDriver driver, String accessibilityId, String message){

     try {
         MobileElement element= (MobileElement) driver.findElementByAccessibilityId(accessibilityId);
         if(element.isEnabled() || element.isDisplayed()){
             retur`n  true;

         }

     }catch (Exception e){
         Assert.fail(message);
     }
     Assert.fail(message);
     return false;
 }


 public  void ClickElement(IOSDriver driver, String accessibilityId, String message){

   if (CheckElement(driver,accessibilityId,message)){

       driver.findElementByAccessibilityId(accessibilityId).click();
   }

 }

public void TypeData(IOSDriver driver, String accessibilityId, String data, String message){

  try {
    if (CheckElement(driver,accessibilityId,message)){

       driver.findElementByAccessibilityId(accessibilityId).sendKeys(data);
    }
  }catch (Exception e){

      Assert.fail(message);
  }


}

}
