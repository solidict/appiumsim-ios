import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverForIOSPlatform {

public IOSDriver IosDevice()throws MalformedURLException {
    IOSDriver driver;
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("platformName", iOSConstants.PLATFORM_NAME);
    capabilities.setCapability("deviceName", iOSConstants.DEVICE_NAME);
    capabilities.setCapability("deviceType", iOSConstants.DEVICE_TYPE);
    capabilities.setCapability("bundleId", iOSConstants.BUNDLE_ID);
    capabilities.setCapability("automationName", iOSConstants.AUTOMATION_NAME);
    capabilities.setCapability("clearSystemFiles", true);
    capabilities.setCapability("platformVersion", iOSConstants.PLATFORM_VERSION);
    capabilities.setCapability("udid", iOSConstants.UDID);

    driver = new IOSDriver(new URL(iOSConstants.URL), capabilities);

    return driver;






}


}
