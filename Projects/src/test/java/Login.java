import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class Login extends Data.iOSProjectData {
    AccessibilityIdMethods accessibilityIdMethods=new AccessibilityIdMethods();
    DriverForIOSPlatform iosDriver= new DriverForIOSPlatform();
    IOSDriver driver;

 @Test
  public void Setup(){
      try {
         driver= iosDriver.IosDevice();
         driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
      } catch (MalformedURLException e) {
          e.printStackTrace();
      }

  }

@Test(dependsOnMethods = {"Setup"})
public void ClickGirisYapButton(){
    //Click Giris Yap Button
    accessibilityIdMethods.ClickElement(driver, accessiablityID_GirisYap_Button,"Giris Yap butonu tespit edilememistir!");

}

@Test(dependsOnMethods = {"ClickGirisYapButton"})
    public void TypeGSMNumber(){

    accessibilityIdMethods.TypeData(driver,accessiablityID_GSMNumber, GSMNumber, "GSM Numarasi giris alani tespit edilememistir!" );
}

@Test(dependsOnMethods = {"TypeGSMNumber"})
    public void TypePassword(){
    accessibilityIdMethods.TypeData(driver,accessiablityID_GSM_Password,password,"Sifre giris alani tespit edilememistir!" );
}

@Test(dependsOnMethods = {"TypePassword"})
    public void ClickLoginButton(){

     accessibilityIdMethods.ClickElement(driver,accessiablityID_Login_Splash_JPG, "Bos alana tiklamada hata meydana gelmistir!");

    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
   accessibilityIdMethods.ClickElement(driver,accessiablityID_Login_Button,"Login icin Giris Yap butonu tespit edilememistir!");

}

@Test(dependsOnMethods = {"ClickLoginButton"})
    public void LoginControl(){
driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
accessibilityIdMethods.CheckElement(driver,accessiablityID_Firsatlarim,"Kullanici login olamamistir!");

}



}
