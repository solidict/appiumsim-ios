import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class Logout extends Data.iOSProjectData {

    AccessibilityIdMethods accessibilityIdMethods=new AccessibilityIdMethods();
    DriverForIOSPlatform iosDriver= new DriverForIOSPlatform();
    IOSDriver driver;

    @Test
    public void Setup(){
        try {
            driver= iosDriver.IosDevice();
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Test(dependsOnMethods = {"Setup"})
    public void FirsatlarimIconControl(){

      Boolean isLogin= accessibilityIdMethods.CheckElement(driver, accessiablityID_Firsatlarim, "Firsatlarim ikonu tespit edilememistir!" );

     if (!isLogin){
         Assert.fail("Kullanici zaten Logout durumdadir!");
     }
    }

    @Test(dependsOnMethods = {"FirsatlarimIconControl"})
    public void ClickMenuIcon(){
    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    accessibilityIdMethods.ClickElement(driver, accessiabilityID_MenuIcon, "Menu ikonu tespit edilememistir!");
    }

   @Test(dependsOnMethods = {"ClickMenuIcon"})
   public void ClickCikisButton(){

        accessibilityIdMethods.ClickElement(driver,accessiabilityID_CikisButton, "Cikis butonu tespit edilememistir!");


   }

   @Test(dependsOnMethods = {"ClickCikisButton"})`
    public void ClickPopUpCikisYapButton(){

       driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
       accessibilityIdMethods.ClickElement(driver,accesiabilityID_PopUpCikisYapButton,"Cikis Yap butonu tespit edilememistir!");

   }

   @Test(dependsOnMethods = {"ClickPopUpCikisYapButton"})
    public void LogoutControl(){

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60,TimeUnit.SECONDS);
        accessibilityIdMethods.CheckElement(driver, accessiablityID_GirisYap_Button, "Logou isleminden sonra Giris Yap butonu tespit edilememistir!");

   }

}
