import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Deneme {


    IOSDriver driver;

    @Before
    public void setup() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", iOSConstants.PLATFORM_NAME);
        capabilities.setCapability("deviceName", iOSConstants.DEVICE_NAME);
        capabilities.setCapability("deviceType", iOSConstants.DEVICE_TYPE);
        capabilities.setCapability("bundleId", iOSConstants.BUNDLE_ID);
        capabilities.setCapability("automationName", iOSConstants.AUTOMATION_NAME);
        capabilities.setCapability("clearSystemFiles", true);
        capabilities.setCapability("platformVersion", iOSConstants.PLATFORM_VERSION);
        capabilities.setCapability("udid", iOSConstants.UDID);
        driver = new IOSDriver(new URL(iOSConstants.URL), capabilities);

        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
//        driver.quit();
    }

    @Test
    public void Login() {
        // login splash
        driver.findElementByAccessibilityId("loginSplashLogin").click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // login
        MobileElement phoneTextfield = (MobileElement) driver.findElementByAccessibilityId("phoneTextField");
        phoneTextfield.sendKeys(iOSConstants.MSISDN);
        MobileElement passwordTextfield = (MobileElement) driver.findElementByAccessibilityId("passwordTextField");
        passwordTextfield.sendKeys(iOSConstants.PASSWORD);
        driver.hideKeyboard();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElementByAccessibilityId("buttonForLogin").click();

        // pohpohla
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElementByAccessibilityId("pohPohlaButton").click();
    }

    }
